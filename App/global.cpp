/**
 * @file global.cpp
 * @brief Definition of global data.
 * @author Tomas Jakubik
 * @date Sep 17, 2020
 */

#include <global.hpp>

//Global objects
Global global;  ///< Structure for interrupt-main communication




