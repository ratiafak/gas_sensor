/**
 * @file app_state.hpp
 * @brief Helper template with state machine skeleton.
 * @author Tomas Jakubik
 * @date Jan 14, 2021
 */

#ifndef APP_STATE_HPP_
#define APP_STATE_HPP_

#include <cstdint>
#include <stm32l0xx.h>

/**
 * @brief Helper template with state machine skeleton.
 * @tparam T enum with available states
 */
template <typename T>
class AppState
{
    uint32_t timeout;   ///< State should end before this time
    uint32_t start;     ///< Time when this state became active
    T state;            ///< Current state
    T previous_state;   ///< Previous state

public:
    AppState() :
        timeout(0), start(0)
    {
    }

    virtual ~AppState(){}

    /**
     * @brief Change to a new state.
     * @param new_state
     * @param set_timeout time limit for this state
     */
    void set(T new_state, uint32_t set_timeout = 0)
    {
        previous_state = state;
        state = new_state;
        start = HAL_GetTick();
        timeout = set_timeout;
    }

    /**
     * @brief How long this state is running.
     * @return time since set [ms]
     */
    uint32_t passed()
    {
        return (HAL_GetTick() - start);
    }

    /**
     * @brief Know whether state timeouts.
     * @return true if timeout has passed
     */
    bool isOverdue()
    {
        return ((HAL_GetTick() - start) > timeout);
    }

    /**
     * @brief Get current state.
     * @return one possible state
     */
    T getState() const
    {
        return state;
    }

    /**
     * @brief Get previous state.
     * @return one possible state
     */
    T getPreviousState() const
    {
        return previous_state;
    }
};



#endif /* APP_STATE_HPP_ */
