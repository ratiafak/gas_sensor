/**
 * @file app.cpp
 * @brief Application behavior.
 * @author Tomas Jakubik
 * @date Aug 28, 2020
 */

#include <app.hpp>
#include <global.hpp>

#include <Hdc2080.h>
#include <Lmp91000.h>
#include <Threshold.h>
#include <S2LP_Config.h>

Hdc2080 hdc(global.hi2c1, HDC2080_ADDRESS, Hdc2080::Precision::p09b, Hdc2080::Precision::p09b);  ///< Temperature and humidity
Lmp91000 lmp(global.hi2c1);    ///< Gas sensor AFE

/**
 * @brief Initialize external components and application.
 */
void app_init()
{
    //Stop and blink if this is an old hardware
    if ((global.id.getShortId() > 0) && (global.id.getShortId() < 5))
    {
        LL_GPIO_SetPinMode(GPIOA, GPIO_PIN_6, LL_GPIO_MODE_OUTPUT);
        while(1)
        {
            HAL_Delay(100);
            LL_GPIO_SetOutputPin(GPIOA, GPIO_PIN_6);
            HAL_Delay(100);
            LL_GPIO_ResetOutputPin(GPIOA, GPIO_PIN_6);
        }
    }

    //Write calibration
    volatile bool write = false;
    if(write)
    {
        Calibration::Data data = global.calibration.getDefault();
        switch(global.id.getShortId())
        {
            case 1:
                data.freq_offset = 21400;
                break;
            case 2:
                data.freq_offset = 13900;
                break;
            case 3:
                data.freq_offset = 24600;
                break;
            case 4:
                data.freq_offset = 20815;
                break;
            case 5:
                data.freq_offset = 14800;
                break;
            case 6:
                data.freq_offset = 10300;
                break;
            case 7:
                data.freq_offset = 13200;
                break;
            case 8:
                data.freq_offset = 12300;
                break;
            case 9:
                data.freq_offset = 13100;
                break;
            case 10:
                data.freq_offset = 16900;
                break;
            default:
                data.freq_offset = 0;
                break;
        }
        global.calibration.writeData(data);
    }

    //Calibrate ADC
    global.adc.calibrate();

    //Calibrate LSI time
    if (Sleep::calibrateLsi() == 0)
    {
        while(1)
        {
            __BKPT(0);
        }
    }

    //Initialize temperature and humidity sensor
    if (hdc.init() == false)
    {
        while(1)
        {
            __BKPT(0);
        }
    }

    //Try communication with LMP sensor
    if (lmp.setMode(Lmp91000::Mode::Sleep) == false)
    {
        while (1)
        {
            __BKPT(0);
        }
    }

    //Radio calibration
    volatile int calibrate = 0;
    if(calibrate > 0)
    {
        uint32_t freq = 865000000;
        if (calibrate == 1)
        {
            freq += global.calibration.getData()->freq_offset;
        }
        if (global.radio.cwStart(freq) == false)
        {
            while (1)
            {
                __BKPT(0);
            }
        }

        while(calibrate);   //Wait while transmitting
        //CW stops with init
    }

    //Radio, 64 channels centered into the SDR band
    if (global.radio.init(863542400 + global.calibration.getData()->freq_offset, 38400) == false)
    {
        while (1)
        {
            __BKPT(0);
        }
    }
    global.radio.shutdown();
}

/**
 * @brief Start ADC measurement.
 */
void App::MeasureADCState::transit()
{
    state.set(StateEnum::MeasureADC, 8);  //Timeout is 8 ms

    vbat_measured = (other.state.getPreviousState() != StateEnum::SetDAC)   //Not after DAC tweak
        && ((vbat_next_measure_time <= Sleep::getLpTime())  //Time to measure
            || (other.threshold_active));   //If threshold is reached, send always
    global.adc.startMeasurement(vbat_measured);  //Start ADC
}

/**
 * @brief Wait for ADC to measure all channels.
 */
void App::MeasureADCState::loop()
{
    //Check timeout
    if (state.isOverdue())
    {
        global.adc.conversionFailed();
        transit();  //Start measurement again
        return;
    }

    //ADC values ready
    if (global.adc.data_ready.pending())
    {
        if (global.gas_threshold
            || (other.state.getPreviousState() != StateEnum::SetDAC))   //Skip gas measurement if this was DAC calibration
        {
            //Update vbat to be sent
            if(vbat_measured)
            {
                vbat_next_measure_time = Sleep::getLpTime() + Sleep::LpTime(30, 0); //Next measurement will be
                other.transmit.setVbat(global.adc.getVbat());
            }

            //Get measured current
            int32_t gas_current = global.adc.getGasCurrent(
                Lmp91000::tia_ohm(static_cast<Lmp91000::TiaGain>(other.control.tia_gain)),
                Lmp91000::int_z_coeff(static_cast<Lmp91000::IntZ>(other.control.int_z)));

            //Check threshold and switch to secondly
            if(other.control_valid && other.control.threshold_enable
                && (abs(gas_current) >= abs(other.control.threshold)))
            {
                other.threshold_deactivate = false;
                other.threshold_active = true;
            }
            else if(other.threshold_active)
            {
                other.threshold_deactivate = true;  //Mark to deactivate threshold when data are sent
            }

            //Threshold triggered
            if (global.gas_threshold)
            {
                global.gas_threshold = false;

                other.force_send = true;    //Flush fifo of measured data

                other.transmit.prepare_extra(gas_current);  //Send extra packet
                other.transmit();
                return;
            }

            //Store sample
            gas_sum += gas_current;
            gas_sum_cnt++;
            if (other.control.secondly || other.threshold_active)
            {
                if (gas_sum_cnt == AVERAGE_N_SECONDLY)
                {

                    other.second_fifo.put(Sample((gas_sum + (AVERAGE_N_SECONDLY / 2)) / AVERAGE_N_SECONDLY));
                    gas_sum = 0;
                    gas_sum_cnt = 0;
                }
            }
            else
            {
                if (gas_sum_cnt == AVERAGE_N_MINUTELY)
                {
                    other.minute_fifo.put(Sample((gas_sum + (AVERAGE_N_MINUTELY / 2)) / AVERAGE_N_MINUTELY));
                    gas_sum = 0;
                    gas_sum_cnt = 0;
                }
            }
        }

        if (other.control.threshold_enable)
        {
            //Get difference between configured and real threshold
            int32_t real_threshold = global.adc.getGasThreshold(
                Lmp91000::tia_ohm(static_cast<Lmp91000::TiaGain>(other.control.tia_gain)),
                Lmp91000::int_z_coeff(static_cast<Lmp91000::IntZ>(other.control.int_z)));

            //Tweak if it differs by more than 1/8 of threshold
            if (abs(other.control.threshold - real_threshold) > (abs(other.control.threshold) / 16))
            {
                Threshold::disable();
                other.set_dac();    //Tweak threshold voltage and measure again
                return;
            }
            else
            {
                Threshold::enable(other.control.threshold > 0);
            }
        }
        else    //Threshold disabled
        {
            Threshold::disable();
        }

        uint32_t level = other.minute_fifo.level() + other.second_fifo.level();
        if ((level > other.FIFO_SEND_LEVEL)  //Enough LMP data
            || other.force_send
            || ((level > 0) && other.threshold_active))   //Threshold over limit
        {
            other.measure_hdc();  //Measure humidity and temperature and send
        }
        else
        {
            other.sleep();    //Sleep and measure again
        }
    }
}

/**
 * @brief Start DAC conversion.
 */
void App::SetDACState::transit()
{
    state.set(StateEnum::SetDAC);

    Threshold::dac_start((
        global.adc.convertThreshold(other.control.threshold,
                                    Lmp91000::tia_ohm(static_cast<Lmp91000::TiaGain>(other.control.tia_gain)),
                                    Lmp91000::int_z_coeff(static_cast<Lmp91000::IntZ>(other.control.int_z)))
        + 8) / 16);
    start_time = HAL_GetTick();
}

/**
 * @brief Wait a short time and stop DAC.
 */
void App::SetDACState::loop()
{
    if (start_time != HAL_GetTick()) //Time differs, a millisecond has passed
    {
        Threshold::dac_stop();
        other.measure_adc();    //Measure and check set value
    }
}

/**
 * @brief Start temperature and humidity measurement.
 */
void App::MeasureHDCState::transit()
{
    state.set(StateEnum::MeasureHDC, 10);
    hdc.measure();  //Start humidity and temperature measurement
}

/**
 * @brief Wait for HDC to measure temperature and humidity.
 */
void App::MeasureHDCState::loop()
{
    if (state.isOverdue() == false)  //Check timeout
    {
        if (global.hdc_data_ready.pending()) //Humidity and temperature ready
        {
            if (hdc.readout())        //Read values
            {
                //Transmit
                other.transmit.prepare_gather(hdc.getTemperature0C1(), hdc.getHumidityPercent());
                other.transmit();
                return;
            }
        }
        else
        {
            return; //Wait longer
        }
    }

    hdc.init();  //Reinit stuck measurement
    other.measure_hdc();  //Start measurement again
}

/**
 * @brief Prepare packet to transmit.
 * @param temperature temperature [0.1 deg C]
 * @param humidity humidity [% RH]
 */
void App::TransmitState::prepare_gather(int32_t temperature, int32_t humidity)
{
    Sample sample;  //To get samples from fifo
    uint16_t last_timestamp;    //To follow timestamps and difference between them
    uint32_t payload_idx = 0;   //Index into payload array

    attempts = MAX_TX_ATTEMPTS; //Reset attempt counter
    gather_t* gather = reinterpret_cast<gather_t*>(packet);
    other.second_fifo.reset_cond();    //Reset conditional fifo output
    other.minute_fifo.reset_cond();    //Reset conditional fifo output

    //Fill header
    gather->proto.header = HEADER_GATHER;
    gather->proto.id = global.id.getShortId();
    gather->proto.seq = other.seq;

    //Fill vbat and HDC data
    gather->vbat = next_vbat;
    gather->humidity = humidity;
    gather->temperature = temperature;

    //Gather data from secondly fifo
    gather->secondly = 0;
    if (other.second_fifo.look_cond(sample))   //Is there a sample
    {
        other.second_fifo.pop_cond();  //Move fifo output
        last_timestamp = sample.timestamp;
        gather->payload[payload_idx++] = sample.timestamp;   //Timestamp of first sample in series
        gather->payload[payload_idx++] = sample.current;
        gather->secondly++;

        while ((payload_idx < GATHER_PAYLOAD_SIZE)  //Space in buffer
            && (other.second_fifo.look_cond(sample)))  //Is there a sample
        {
            if (sample.timestamp != (last_timestamp + 1))   //Timestamps need to be secondly
            {
                break;
            }

            other.second_fifo.pop_cond();  //Move fifo output
            last_timestamp = sample.timestamp;
            gather->payload[payload_idx++] = sample.current;
            gather->secondly++;
        }
    }

    //Gather data from minutely fifo
    if (payload_idx < (GATHER_PAYLOAD_SIZE - 1))   //There needs to be 2 or more spaces to insert minute samples
    {
        if (other.minute_fifo.look_cond(sample))   //Is there a sample
        {
            other.minute_fifo.pop_cond(); //Move fifo output
            last_timestamp = sample.timestamp;
            gather->payload[payload_idx++] = sample.timestamp;   //Timestamp of first sample in series
            gather->payload[payload_idx++] = sample.current;

            while((payload_idx < GATHER_PAYLOAD_SIZE)  //Space in buffer
                && (other.minute_fifo.look_cond(sample)))  //Is there a sample
            {
                if(sample.timestamp != (last_timestamp + 60))   //Timestamps need to be minutely
                {
                    break;
                }

                other.minute_fifo.pop_cond(); //Move fifo output
                last_timestamp = sample.timestamp;
                gather->payload[payload_idx++] = sample.current;
            }
        }
    }

    //Set packet length
    length = sizeof(gather_t) - (GATHER_PAYLOAD_SIZE - payload_idx) * sizeof(uint16_t);
}

/**
 * @brief Prepare packet to transmit.
 * @param current sensor current [nA]
 */
void App::TransmitState::prepare_extra(int32_t current)
{
    attempts = MAX_TX_ATTEMPTS_EXTRA; //Reset attempt counter
    extra_t* extra = reinterpret_cast<extra_t*>(packet);

    //Fill header
    extra->proto.header = HEADER_EXTRA;
    extra->proto.id = global.id.getShortId();
    extra->proto.seq = other.seq;

    //Fill extra sample
    Sleep::LpTime timestamp = Sleep::getLpTime();
    extra->nearest_timestamp = timestamp.minute * 60 + timestamp.subsecond / Sleep::SUBSECONDS_N;
    extra->extra_seq = other.extra_seq++;
    extra->lmp = current;

    //Set packet length
    length = sizeof(extra_t);
}

/**
 * @brief Prepare packet to transmit.
 */
void App::TransmitState::prepare_request()
{
    attempts = MAX_TX_ATTEMPTS; //Reset attempt counter
    request_t* request = reinterpret_cast<request_t*>(packet);

    //Fill header
    request->proto.header = HEADER_REQUEST;
    request->proto.id = global.id.getShortId();
    request->proto.seq = other.seq;

    //Fill firmware name
    memset(request->fw_ver, '\0', sizeof(request->fw_ver));
    strncpy(request->fw_ver, FW_VER, sizeof(request->fw_ver));

    //Set packet length
    length = sizeof(request_t);
}

/**
 * @brief Set radio to transmit a packet
 */
void App::TransmitState::transit()
{
    if ((attempts > 0) && (length > 0))    //Some attempts left
    {
        attempts--;
        state.set(StateEnum::Transmit, 100);  //Set with timeout
        LL_GPIO_SetOutputPin(LED_R_GPIO_Port, LED_R_Pin);  //Red LED during transmitting

        //Set channel
        other.channel = other.control.multi_f ? mers_mlcg_31b_rand(&global.mers_mlcg) & 0x3f : FREQ_CHANNEL_FIRST;

        //Response is expected on the same channel
        switch (reinterpret_cast<proto_t*>(packet)->header)
        {
            case HEADER_REQUEST: reinterpret_cast<request_t*>(packet)->channel = other.channel; break;
            case HEADER_GATHER:  reinterpret_cast<gather_t*>(packet)->channel  = other.channel; break;
            case HEADER_EXTRA:   reinterpret_cast<extra_t*>(packet)->channel   = other.channel; break;
            default: other.sleep(); break;
        }

        //Send
        uint32_t wait = (mers_mlcg_31b_rand(&global.mers_mlcg) & 0xf) + 1;  //1 ~ 16
        if (wait > 15)
        {
            wait = 15; //Replace 16 with 15, this non-uniformity is neglected
        }
        global.radio.send(packet, length, other.channel, wait);
    }
    else
    {
        other.sleep();  //Give up and sleep
    }
}

/**
 * @brief Wait for packet to be transmitted.
 */
void App::TransmitState::loop()
{
    //Check timeout
    if (state.isOverdue())
    {
        other.sleep();  //Sleep and measure again
        return;
    }

    Radio::Result res;
    if ((res = global.radio.poolResult()) != Radio::Result::Working)
    {
        LL_GPIO_ResetOutputPin(LED_R_GPIO_Port, LED_R_Pin);  //Led off
        if (res == Radio::Result::TxSent)  //Successful Tx
        {
            other.receive();  //Receive
        }
        else
        {
            transit();  //Repeat Tx
        }
    }
}

/**
 * @brief Reconfigure LMP if necessary.
 * @param config new config
 * @return true on success
 */
bool App::ReceiveState::lmp_config(const control_t &config)
{
    static control_t last = { 0 };  //Last configuration

    //Check validity, other items have no invalid states
    if (config.bias > static_cast<uint16_t>(Lmp91000::Bias::p24))
    {
        return false;
    }

    //Check whether config changed
    if ((config.tia_gain               != last.tia_gain           )
        || (config.r_load              != last.r_load             )
        || (config.ref_source_external != last.ref_source_external)
        || (config.int_z               != last.int_z              )
        || (config.bias_positive       != last.bias_positive      )
        || (config.bias                != last.bias               ))    //Anything changed
    {
        if (lmp.configure(static_cast<Lmp91000::TiaGain>(config.tia_gain),
                          static_cast<Lmp91000::RLoad>(config.r_load),
                          config.ref_source_external,
                          static_cast<Lmp91000::IntZ>(config.int_z),
                          config.bias_positive,
                          static_cast<Lmp91000::Bias>(config.bias)))   //Config
        {
            last = config;  //Store for future reference

            if (lmp.getMode() != Lmp91000::Mode::On3Lead)
            {
                lmp.setMode(Lmp91000::Mode::On3Lead);
            }

            return true;
        }
        return false;
    }
    return true;
}

/**
 * @brief Receive packet and apply config.
 * @return true on success
 */
bool App::ReceiveState::receive_packet()
{
    //Get received packet
    control_t packet; //ACK packet will fit into this
    uint32_t len = global.radio.getReceivedData(reinterpret_cast<uint8_t*>(&packet), sizeof(control_t));

    //Check header
    if (((packet.proto.header != HEADER_ACK) && (packet.proto.header != HEADER_CONTROL))    //Only interested in these two
        || (packet.proto.id != global.id.getShortId())  //Not for me
        || (packet.proto.seq != other.seq)    //Seq number must fit with packet transmitted
        || ((packet.proto.header == HEADER_ACK) && (len != sizeof(ack_t)))  //Check ack length
        || ((packet.proto.header == HEADER_CONTROL) && (len != sizeof(control_t)))) //Check control length
    {
        return false;
    }

    //Increment seq for next packet
    other.seq = (other.seq + 1) & 0x3;

    //Data were acked, disable threshold
    if (other.threshold_deactivate)
    {
        other.threshold_active = false;
        other.threshold_deactivate = false;
    }

    //Flush acked data from fifo
    other.second_fifo.flush_cond();
    other.minute_fifo.flush_cond();
    if (other.minute_fifo.level() + other.second_fifo.level() == 0) //All sent
    {
        other.force_send = false;
    }

    //Process control
    if (packet.proto.header == HEADER_CONTROL)
    {
        other.control_valid = true;
        other.control = packet;  //Store
        lmp_config(other.control);  //Reconfigure LMP
    }

    return true;
}

/**
 * @brief Start receiving.
 */
void App::ReceiveState::transit()
{
    state.set(StateEnum::Receive, 100);  //Set with timeout
    global.radio.receive(20, other.channel);
}

/**
 * @brief Wait for received packet.
 */
void App::ReceiveState::loop()
{
    //Check timeout
    if (state.isOverdue())
    {
        other.sleep();  //Sleep and measure again
        return;
    }

    Radio::Result res;
    if ((res = global.radio.poolResult()) != Radio::Result::Working)
    {
        if (res == Radio::Result::RxReceived)  //Received
        {
            if (receive_packet())  //Get and verify packet
            {
                other.sleep();  //Sleep until next sample
                return;
            }
        }

        other.transmit();  //Repeat Tx
    }
}

/**
 * @brief Set interrupt and switch to sleep.
 */
void App::SleepState::transit()
{
    state.set(StateEnum::Sleep);
    global.radio.shutdown();  //Radio to lowest power
    if (other.control.secondly || other.threshold_active)
    {
        to = Sleep::getLpTime() + Sleep::LpTime(0, 1);  //Sleep until next Sleep::SUBSECONDS_Nth of a second
    }
    else
    {
        to = Sleep::getLpTime();
        const constexpr uint32_t SAMPLE_TIME = (60 * Sleep::SUBSECONDS_N / AVERAGE_N_MINUTELY);
        to.subsecond = ((to.subsecond / SAMPLE_TIME) + 1) * SAMPLE_TIME;  //Add one AVERAGE_Nth of a minute
    }
    Sleep::setInterrupt(to);  //Set interrupt
}

/**
 * @brief Sleep until given time and then switch to MeasureADC.
 */
void App::SleepState::loop()
{
    while (other.control_valid == false)    //Got to this state without getting config
    {
        Sleep::sleep(); //Play dead
    }

    Sleep::LpTime now = Sleep::getLpTime();
    if ((now < to) && (global.gas_threshold == false))  //Still sleep
    {
        Sleep::sleep();  //Sleep until given time
    }
    else
    {
        other.measure_adc();    //Start measurement
    }
}

#include <FastTimer.h>
/**
 * @brief Main application loop.
 */
void app_loop()
{
    App app;
    app.transmit.prepare_request();
    app.transmit();  //Start by asking for config

    while (1)
    {
        //Loop states
        switch (app.state.getState())
        {
            case App::StateEnum::MeasureADC: app.measure_adc.loop(); break;
            case App::StateEnum::SetDAC:     app.set_dac.loop();     break;
            case App::StateEnum::MeasureHDC: app.measure_hdc.loop(); break;
            case App::StateEnum::Transmit:   app.transmit.loop();    break;
            case App::StateEnum::Receive:    app.receive.loop();     break;
            case App::StateEnum::Sleep:      app.sleep.loop();       break;
            default:                         NVIC_SystemReset();     break;
        }

        //Wait until next systick
        __WFI();
    }
}




