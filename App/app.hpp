/**
 * @file app.hpp
 * @brief Application definitions and global variables.
 * @author Tomas Jakubik
 * @date Aug 28, 2020
 */


#ifndef APP_HPP_
#define APP_HPP_

#include <app_state.hpp>
#include <packet.h>
#include <Sleep.h>
#include <Utils.hpp>

#define FW_VER    "S01.00.0012" //Firmware version of Sensor Smajor.minor.patch

/**
 * @brief Initialize external components and application.
 */
void app_init();

/**
 * @brief Main application loop.
 */
void app_loop();

/**
 * @brief Main state machine class.
 */
class App
{
public:
    ///Global states
    enum class StateEnum
    {
        Init = 0,   ///< Not a valid state
        MeasureADC, ///< Measure ADC
        SetDAC,     ///< Set DAC for a short period and tweak threshold
        MeasureHDC, ///< Measure temperature and humidity
        Transmit,   ///< Transmit packet
        Receive,    ///< Receive response
        Sleep,      ///< Sleep until next measure
    };
    AppState<StateEnum> state;  ///< State machine state

    ///Prototype of nested class for single state
    struct NestedSingleState {
        App& other; ///< Access to other states
        AppState<StateEnum> &state; ///< State machine control
        NestedSingleState(App& x): other(x), state(x.state) {}
        virtual void transit() = 0; ///< Transit to this state
        virtual void loop() = 0;    ///< Loop this state
        void operator()(){ transit(); }   ///< Call state to transit
    };

    control_t control;  ///< Settings for measurement
    bool control_valid = false; ///< True when a config was obtained from CU
    uint32_t seq = 0;   ///< Sequence number, incremented when packet is acked
    uint32_t extra_seq = 0;    ///< Sequence number, incremented on unique extra samples
    uint32_t channel = 0;   ///< Channel used to transmit

    ///Structure of measured value and its dating
    struct Sample
    {
        uint16_t timestamp; ///< Time of the sample [s]
        int16_t current;    ///< Measured LMP current [nA]
        Sample() : timestamp(0), current(0) {}
        Sample(Sleep::LpTime timestamp, int16_t current) :
            timestamp(timestamp.minute * 60 + timestamp.subsecond / Sleep::SUBSECONDS_N), current(current) {}
        Sample(int16_t current) : Sample(Sleep::getLpTime(), current) {}
    };
    Fifo<Sample, 30> minute_fifo;   ///< Fifo for minute interval samples
    Fifo<Sample, 30> second_fifo;   ///< Fifo for second interval samples
    static const constexpr int FIFO_SEND_LEVEL = 25;   ///< Send data if more than this in fifos
    static const constexpr uint32_t AVERAGE_N_SECONDLY = Sleep::SUBSECONDS_N;  ///< Average this many samples for one sent value
    static const constexpr uint32_t AVERAGE_N_MINUTELY = 8;  ///< Average this many samples for one sent value

    bool threshold_active = false;  ///< True when threshold is active
    bool threshold_deactivate = false;  ///< True to deactivate threshold when data are sent
    bool force_send = false;    ///< True to force sending all from fifo

    ///Data for measure ADC state
    struct MeasureADCState: public NestedSingleState {
        using NestedSingleState::NestedSingleState;
        void transit() override;    ///< Transit to this state
        void loop() override;       ///< Loop this state

        bool vbat_measured = false; ///< Marked when Vbat is measured
        Sleep::LpTime vbat_next_measure_time = Sleep::LpTime(0, 0); ///< Time when to measure vbat
        int32_t gas_sum = 0;    ///< Sum gas current values for averaging
        uint32_t gas_sum_cnt = 0;   ///< Count subsamples and average
    } measure_adc = MeasureADCState(*this);

    ///Data for setDAC state
    struct SetDACState: public NestedSingleState {
        using NestedSingleState::NestedSingleState;
        void transit() override;    ///< Transit to this state
        void loop() override;       ///< Loop this state

        uint32_t start_time = 0;    ///< HAL time of DAC start
    } set_dac = SetDACState(*this);

    ///Data for measure temperature and humidity state
    struct MeasureHDCState: NestedSingleState {
        using NestedSingleState::NestedSingleState;
        void transit() override;    ///< Transit to this state
        void loop() override;       ///< Loop this state
    } measure_hdc = MeasureHDCState(*this);

    ///Data for transmit state
    struct TransmitState: NestedSingleState {
        using NestedSingleState::NestedSingleState;
        void transit() override;    ///< Transit to this state
        void loop() override;       ///< Loop this state

        /**
         * @brief Set vbat to be sent in next gather packet.
         * @param vbat battery voltage [mV]
         */
        void setVbat(uint16_t vbat) {next_vbat = vbat;}

        /**
         * @brief Prepare packet to transmit.
         * @param temperature temperature [0.1 deg C]
         * @param humidity humidity [% RH]
         */
        void prepare_gather(int32_t temperature, int32_t humidity);

        /**
         * @brief Prepare packet to transmit.
         * @param current sensor current [nA]
         */
        void prepare_extra(int32_t current);

        /**
         * @brief Prepare packet to transmit.
         */
        void prepare_request();

        static const constexpr uint32_t MAX_TX_ATTEMPTS = 5;   ///< Number of attempts to transmit
        static const constexpr uint32_t MAX_TX_ATTEMPTS_EXTRA = 15;   ///< Number of attempts to transmit
        uint32_t attempts = 0;  ///< Remaining number of transmit attempts before giving up
        uint16_t next_vbat = 0; ///< vbat value to be used in next gather packet
        uint8_t packet[sizeof(gather_t)] = { 0 };   ///< Packet to be sent
        uint32_t length = 0;    ///< Size of the gather packet
    } transmit = TransmitState(*this);

    ///Data for receive state
    struct ReceiveState: NestedSingleState {
        using NestedSingleState::NestedSingleState;
        void transit() override;    ///< Transit to this state
        void loop() override;       ///< Loop this state

        /**
         * @brief Reconfigure LMP if necessary.
         * @param config new config
         * @return true on success
         */
        bool lmp_config(const control_t &config);

        /**
         * @brief Receive packet and apply config.
         * @return true on success
         */
        bool receive_packet();
    } receive = ReceiveState(*this);

    ///Data for sleep state
    struct SleepState: NestedSingleState {
        using NestedSingleState::NestedSingleState;
        void transit() override;    ///< Transit to this state
        void loop() override;       ///< Loop this state

        Sleep::LpTime to = Sleep::LpTime(0, 0); ///< Sleep until this time
    } sleep = SleepState(*this);

    App()
    {
        control.multi_f = 0;    //Disable frequency switching by default
    }
};

#endif /* APP_HPP_ */
