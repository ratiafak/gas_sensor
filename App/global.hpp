/**
 * @file global.hpp
 * @brief Definition of global data.
 * @author Tomas Jakubik
 * @date Sep 17, 2020
 */


#ifndef GLOBAL_HPP_
#define GLOBAL_HPP_


//MCU pinout definitions
#include <pinout.h>

//Hal and LL includes
#include <stm32l0xx.h>
#include <stm32l0xx_hal.h>
#include "stm32l0xx_ll_comp.h"
#include "stm32l0xx_ll_lptim.h"
#include "stm32l0xx_ll_bus.h"
#include "stm32l0xx_ll_cortex.h"
#include "stm32l0xx_ll_rcc.h"
#include "stm32l0xx_ll_system.h"
#include "stm32l0xx_ll_utils.h"
#include "stm32l0xx_ll_pwr.h"
#include "stm32l0xx_ll_gpio.h"
#include "stm32l0xx_ll_dma.h"
#include "stm32l0xx_ll_exti.h"
#include "stm32l0xx_ll_crs.h"
#include "stm32l0xx_ll_tim.h"
#include "stm32l0xx_ll_rng.h"

//Global objects
#include <Adc.h>
#include <Radio.h>
#include <Id.hpp>
#include <Calibration.h>
#include <mers_mlcg.h>

//Utility classes
#include <Utils.hpp>

//Global interrupt-main communication
struct Global
{
    ///HAL global handles
    ADC_HandleTypeDef hadc;
    DMA_HandleTypeDef hdma_adc;
    DAC_HandleTypeDef hdac;
    I2C_HandleTypeDef hi2c1;
    IWDG_HandleTypeDef hiwdg;
    SPI_HandleTypeDef hspi1;

    Task hdc_data_ready;    ///< Marks when HDC2080 has new data measured
    bool gas_threshold; ///< Marked when gas threshold was reached
    Id id;  ///< Unique identifier of the MCU
    Calibration calibration;    ///< Calibrations stored in EEPROM
    Adc adc = Adc(hadc);    ///< Start ADC measurement and convert values, needs to be called from interrupt
    Radio radio = Radio(hspi1);   ///< Radio driver, needs to be called from interrupt
    mers_mlcg_state_t mers_mlcg;    ///< Random number generator
};

extern Global global;

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void);


#endif /* GLOBAL_HPP_ */
