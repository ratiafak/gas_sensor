/**
 * @file tests.cpp
 * @brief Some tests for the gas sensor.
 * @author Tomas Jakubik
 * @date Mar 3, 2021
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include <catch2/catch.hpp>

#include <cstdint>
#include <Utils.hpp>
#include <Adc.h>

/**
 * @brief Convert AD to current using double calculations.
 * @param raw_vref raw vref AD value (16 bit with oversampling)
 * @param raw_vout raw LMP output pin AD value (16 bit with oversampling)
 * @param raw_vgas raw LMP reference pin AD value (16 bit with oversampling)
 * @param vrefint_cal_3000 factory AD calibration value
 * @param rtia tia resistance [Ohm]
 * @param int_z ratio of voltage on internal zero to LMP reference [%]
 * @return current to WE pin [nA]
 */
double precise_current_conversion(uint16_t raw_vref,
                                  uint16_t raw_vout,
                                  uint16_t raw_vgas,
                                  int32_t vrefint_cal_3000,
                                  int32_t rtia,
                                  int32_t int_z)
{
    double vmcu = static_cast<double>(vrefint_cal_3000) / (raw_vref / 16.0);    //ADC reference voltage [mV]
    double vgas = (static_cast<double>(raw_vgas / 16.0) * vmcu) / 4095;         //Voltage on LMP reference [mV]
    double vout = (static_cast<double>(raw_vout / 16.0) * vmcu) / 4095;         //Voltage on output of LMP [mV]

    double vtia = vgas * int_z / 100;  //Voltage on TIA positive reference [mV]

    return 1000000.0 * (vout - vtia) / static_cast<double>(rtia); //Current to WE [nA]
}

/**
 * @brief Check one set of values.
 * @param vrefint_cal_3000 factory AD calibration value
 * @param rtia tia resistance [Ohm]
 * @param int_z voltage on internal zero [mV] as ratio of 2.5 V
 */
void check_set(int32_t vrefint_cal_3000, int32_t rtia, int32_t int_z)
{
    double max_abs_diff = 0;
    int32_t max_bck_diff = 0;

    for (uint32_t raw_vgas = 16; raw_vgas < (1 << 16); raw_vgas += 3441)        //Reduced space by approx 16*215
    {
        for (uint32_t raw_vref = 16; raw_vref < (1 << 16); raw_vref += 3441)    //Reduced space by approx 16*215
        {
            for (uint32_t raw_vout = 0; raw_vout < (1 << 16); raw_vout += 15)   //Reduced space by approx 16
            {
                INFO("raw_vgas = " << raw_vgas << " raw_vref = " << raw_vref << " raw_vout = " << raw_vout);
                double precise = precise_current_conversion(raw_vref, raw_vout, raw_vgas, vrefint_cal_3000, rtia, int_z);
                int32_t regular = Adc::convertAdToCurrent(raw_vref, raw_vout, raw_vgas, vrefint_cal_3000, rtia, int_z);
                int32_t backwards = Adc::convertCurrentToAd(raw_vref, raw_vgas, round(precise), vrefint_cal_3000, rtia, int_z);

                //Absolute difference
                double abs_diff = precise - regular;
                if(abs_diff < 0)
                {
                    abs_diff =  -1.0*abs_diff;
                }
                if (abs_diff > max_abs_diff)
                {
                    max_abs_diff = abs_diff;
                }
                REQUIRE(abs_diff < 1);

                //Backwards difference
                int32_t bck_diff = raw_vout - backwards;
                if(bck_diff < 0)
                {
                    bck_diff = -1*bck_diff;
                }
                if (bck_diff > max_bck_diff)
                {
                    max_bck_diff = bck_diff;
                }
                REQUIRE(bck_diff < 16);
            }
        }
    }

    WARN("Max abs difference is " << max_abs_diff);
    WARN("Max backwards difference is " << max_bck_diff);
}

/**
 * @brief Compare double calculation to integer in AD current conversion.
 */
TEST_CASE( "AD conversion of gas current", "[gas ad]" )
{
    check_set(5010000, 350000, 50);

    check_set(5010000,   2750, 50); //Smallest R_TIA

    check_set(9156000, 350000, 67); //Somewhat larger calibration constant and large reference value

    check_set(4000000, 350000, 67); //Somewhat smaller calibration constant and large reference value
}

/**
 * @brief Test fifo.
 */
TEST_CASE( "Fifo", "[fifo]" )
{
    Fifo<uint8_t, 6> fifo;

    //Fill
    REQUIRE(fifo.level() == 0);
    REQUIRE(fifo.put(1) == true);
    REQUIRE(fifo.level() == 1);
    REQUIRE(fifo.put(2) == true);
    REQUIRE(fifo.level() == 2);
    REQUIRE(fifo.put(3) == true);
    REQUIRE(fifo.level() == 3);
    REQUIRE(fifo.put(4) == true);
    REQUIRE(fifo.level() == 4);
    REQUIRE(fifo.put(5) == true);
    REQUIRE(fifo.level() == 5);
    REQUIRE(fifo.put(6) == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.put(7) == false);
    REQUIRE(fifo.level() == 6);

    //Empty by conditional pops
    REQUIRE(fifo.pop_cond() == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.pop_cond() == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.pop_cond() == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.pop_cond() == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.pop_cond() == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.pop_cond() == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.pop_cond() == false);
    REQUIRE(fifo.level() == 6);
    fifo.flush_cond();

    //Fill
    REQUIRE(fifo.level() == 0);
    REQUIRE(fifo.put(1) == true);
    REQUIRE(fifo.level() == 1);
    REQUIRE(fifo.put(2) == true);
    REQUIRE(fifo.level() == 2);
    REQUIRE(fifo.put(3) == true);
    REQUIRE(fifo.level() == 3);
    REQUIRE(fifo.put(4) == true);
    REQUIRE(fifo.level() == 4);
    REQUIRE(fifo.put(5) == true);
    REQUIRE(fifo.level() == 5);
    REQUIRE(fifo.put(6) == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.put(7) == false);
    REQUIRE(fifo.level() == 6);

    //Conditionally take, reset and empty by regular pop
    REQUIRE(fifo.pop_cond() == true);
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.pop_cond() == true);
    REQUIRE(fifo.level() == 6);
    fifo.reset_cond();
    REQUIRE(fifo.level() == 6);
    REQUIRE(fifo.pop() == true);
    REQUIRE(fifo.level() == 5);
    REQUIRE(fifo.pop() == true);
    REQUIRE(fifo.level() == 4);
    REQUIRE(fifo.pop() == true);
    REQUIRE(fifo.level() == 3);
    REQUIRE(fifo.pop() == true);
    REQUIRE(fifo.level() == 2);
    REQUIRE(fifo.pop() == true);
    REQUIRE(fifo.level() == 1);
    REQUIRE(fifo.pop() == true);
    REQUIRE(fifo.level() == 0);
    REQUIRE(fifo.pop() == false);
    REQUIRE(fifo.level() == 0);

}

