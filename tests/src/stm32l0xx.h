/**
 * @file stm32l0xx.h
 * @brief Mock file for test.
 * @author Tomas Jakubik
 * @date Mar 18, 2021
 */

#ifndef STM32L0XX_H_
#define STM32L0XX_H_


#define __disable_irq()
#define __enable_irq()

#define ADC_CHANNEL_0           0
#define ADC_CHANNEL_3           0
#define ADC_CHANNEL_4           0
#define ADC_CHANNEL_6           0
#define ADC_CHANNEL_7           0
#define ADC_CHANNEL_VREFINT     0

#define ADC_HandleTypeDef uint32_t

#endif /* STM32L0XX_H_ */
