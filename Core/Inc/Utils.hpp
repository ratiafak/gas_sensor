/**
 * @file Utils.hpp
 * @brief Some useful C++ utils.
 * @author Tomas Jakubik
 * @date Oct 1, 2020
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <cstdint>
#include <cstddef>
#include <stm32l0xx.h>


///Class to trigger main tasks
class Task
{
    bool triggered = false; ///< True if the task is pending

public:
    /**
     * @brief Trigger task to be run.
     */
    void trigger()
    {
        triggered = true;
    }

    /**
     * @brief Return true if task should be run.
     * @return true when task is pending, false if nothing to do
     */
    bool pending()
    {
        if(triggered)
        {
            triggered = false;
            return true;
        }
        return false;
    }
};

///Class to disable interrupts around critical sections
class Critical
{
    static uint32_t count;  ///< Keep track of nested critical sections, initialized to 0 in Utils.cpp
    bool disabled = false;  ///< State of this critical section

public:
    Critical& operator=(const Critical& other) = delete;  ///< Critical cannot be copied
    Critical(const Critical& other) = delete;  ///< Critical cannot be copied

    /**
     * @brief Nestable Critical section with disabled interrupts.
     */
    Critical()
    {
        enter();
    }

    /**
     * @brief Disable interrupts.
     */
    void enter()
    {
        if(disabled == false)
        {
            __disable_irq();
            count++;

            disabled = true;
        }
    }

    /**
     * @brief Enable interrupts.
     */
    ~Critical()
    {
        exit();
    }

    /**
     * @brief Enable interrupts.
     */
    void exit()
    {
        if(disabled)
        {
            disabled = false;

            count--;
            if(count == 0)
            {
                __enable_irq();
            }
        }
    }
};

/**
 * @brief Simple fifo which overrides when full.
 * Requires one more memory than it can hold elements to simplify.
 * Not thread safe.
 * @tparam T type stored in fifo
 * @tparam size number of elements of T in fifo
 */
template <class T, int size>
class Fifo
{
    T buffer[(size + 1)];   ///< Buffer of size+1 because the last element cannot be used
    int in = 0;         ///< Input
    int out = 0;        ///< Output
    int out_cond = 0;   ///< Conditional output

    /**
     * @brief Increment buffer index.
     * @param idx index to increment
     * @return incremented index
     */
    int inc(int idx)
    {
        if(++idx >= (size + 1))
        {
            return 0;
        }
        return idx;
    }

public:
    Fifo(){}
    virtual ~Fifo(){}

    /**
     * @brief Know how many elements are in fifo.
     * @return number of elements in fifo.
     */
    int level()
    {
        if ((in - out) >= 0)
        {
            return (in - out);
        }
        else
        {
            return (in - out + size + 1);
        }
    }

    /**
     * @brief Get next element in fifo.
     * @param element
     * @return true if element was obtained, false if empty
     */
    bool look(T &element) const
    {
        if(out == in)   //Nothing in fifo
        {
            return false;
        }
        else
        {
            element = buffer[out];  //Get element
            return true;
        }
    }

    /**
     * @brief Throw away one element in fifo.
     * @return true if element was thrown, false if empty
     */
    bool pop()
    {
        if(out == in)   //Nothing in fifo
        {
            return false;
        }
        else
        {
            if(out == out_cond) //Conditional pop is not used
            {
                out_cond = inc(out_cond); //Move to next element to get
            }
            out = inc(out); //Move to next element to get
            return true;
        }
    }

    /**
     * @brief Get next element of conditional output.
     * @param element
     * @return true if element was obtained, false if empty
     */
    bool look_cond(T &element)
    {
        if(out_cond == in)   //Nothing in fifo
        {
            return false;
        }
        else
        {
            element = buffer[out_cond];  //Get element
            return true;
        }
    }

    /**
     * @brief Move the conditional fifo output by one element.
     * Doesn't clear the element from the fifo.
     * @return true if output moved, false if empty
     */
    bool pop_cond()
    {
        if(out_cond == in)   //Nothing in fifo
        {
            return false;
        }
        else
        {
            out_cond = inc(out_cond); //Move to next element to get
            return true;
        }
    }

    /**
     * @brief Flush conditionally obtained elements as if they were normally popped.
     */
    void flush_cond()
    {
        out = out_cond; //Move regular output to conditional position
    }

    /**
     * @brief Reset conditionally obtained elements as nothing had happened.
     */
    void reset_cond()
    {
        out_cond = out; //Reset conditional output to regular position
    }

    /**
     * @brief Insert one element to fifo
     * @param element
     * @return true if successful, false if something was overridden
     */
    bool put(T element)
    {
        buffer[in] = element;   //Store element
        in = inc(in);   //Move to a free position
        if (in == out)   //Overflow, this position is not free
        {
            if(out == out_cond) //Conditional pop is not used
            {
                out_cond = inc(out_cond); //Throw away oldest element
            }
            out = inc(out);   //Throw away oldest element
            return false;
        }
        else
        {
            return true;
        }
    }
};

#endif /* UTILS_HPP_ */
