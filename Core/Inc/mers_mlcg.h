/*
 * mers_mlcg.h
 *
 *  Created on: Nov 22, 2018
 *      Author: Tomas Jakubik
 */

#ifndef MERS_MLCG_H_
#define MERS_MLCG_H_

#include <stdint.h>

#ifdef __cplusplus
 extern "C" {
#endif

typedef struct
{
	uint32_t mlcg_state;	///< State of the mlcg generator
	uint32_t reserve;	///< Reserve bits used to fill the MSb
} mers_mlcg_state_t;	///< Internal state of the generator

/**
 * @brief Initialize the generator
 * @param seed generator seed between 1 and MERS_MLCG_P-1
 * @return internal state of the generator
 */
mers_mlcg_state_t mers_mlcg_srand(uint32_t seed);

/**
 * @brief Generate one pseudorandom number between 1 and MERS_MLCG_P-1.
 * @param state internal state of the generator
 * @return almost 31 bits of pseudorandom number (numbers 0x00000000 and 0x7fffffff cannot happen)
 */
uint32_t mers_mlcg_31b_rand(mers_mlcg_state_t* state);

/**
 * @brief Generate one 32 bit pseudorandom number.
 * @param state internal state of the generator
 * @return one pseudorandom number (numbers 0x80000000, 0xffffffff, 0x7fffffff and 0x00000000 cannot happen)
 */
uint32_t mers_mlcg_rand(mers_mlcg_state_t* state);

#ifdef __cplusplus
}
#endif

#endif /* MERS_MLCG_H_ */
