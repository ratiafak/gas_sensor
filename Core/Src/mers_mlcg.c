/*
 * mers_mlcg.c
 *
 *  Created on: Nov 22, 2018
 *      Author: Tomas Jakubik
 */

#include "mers_mlcg.h"

#define MERS_MLCG_P_MERS   (31)	//Nth Mersenne prime
#define MERS_MLCG_P        ((1ULL << MERS_MLCG_P_MERS) - 1)	//2^N - 1 is often a prime
#define MERS_MLCG_A        62089911ULL   //Suggested by Knuth, found by Fishman and Moore

/**
 * @brief Initialize the generator
 * @param seed generator seed between 1 and MERS_MLCG_P-1
 * @return internal state of the generator
 */
mers_mlcg_state_t mers_mlcg_srand(uint32_t seed)
{
	mers_mlcg_state_t state = {.mlcg_state = 123456, .reserve = 0};	//Backup seed
	seed &= MERS_MLCG_P;	//Clear MSb
	if((seed != 0) && (seed != MERS_MLCG_P)) state.mlcg_state = seed;	//Set seed only if it is valid
	return state;
}

/**
 * @brief Generate one pseudorandom number between 1 and MERS_MLCG_P-1.
 * @param state internal state of the generator
 * @return almost 31 bits of pseudorandom number (numbers 0x00000000 and 0x7fffffff cannot happen)
 */
uint32_t mers_mlcg_31b_rand(mers_mlcg_state_t* state)
{
	uint64_t x = (uint64_t)(state->mlcg_state)*MERS_MLCG_A;	//x_new = x_old*A
	x = (x >> MERS_MLCG_P_MERS) + (x & MERS_MLCG_P);	//x / w + x % w (almost mod P, P = w-1)
	if(x >= MERS_MLCG_P) state->mlcg_state = x-MERS_MLCG_P;	//The rset of mod P
	else state->mlcg_state = x;
	return state->mlcg_state;
}

/**
 * @brief Generate one 32 bit pseudorandom number.
 * @param state internal state of the generator
 * @return one pseudorandom number (numbers 0x80000000, 0xffffffff, 0x7fffffff and 0x00000000 cannot happen)
 */
uint32_t mers_mlcg_rand(mers_mlcg_state_t* state)
{
	if(!(state->reserve & 0x3fffffff))	//No reserve left, mark is above bit 31
	{
		state->reserve = (mers_mlcg_31b_rand(state) << 1) | 0x1;	//Make reserve of 30 random bits + LSb mark
	}
	state->reserve <<= 1;	//New reserve bit to MSb
	return mers_mlcg_31b_rand(state) | (state->reserve & 0x80000000);	//Get one 31 bit random number + MSb out of reserve
}
