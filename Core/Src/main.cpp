/**
 * @file main.cpp
 * @brief Entry point of this application and initialization.
 * @author Tomas Jakubik
 * @date Sep 10, 2020
 */

#include <pinout.h>
#include <stm32l0xx.h>
#include <app.hpp>
#include <global.hpp>

void SystemClock_Config(void);
static void MX_DMA_Init(void);
static void MX_ADC_Init(void);
static void MX_COMP1_Init(void);
static void MX_DAC_Init(void);
static void MX_I2C1_Init(void);
static void MX_IWDG_Init(void);
static void MX_LPTIM1_Init(void);
static void MX_SPI1_Init(void);
static void MX_GPIO_Init(void);
static void MX_RNG_Init(void);

/**
 * @brief Initialize peripherals and call app init and app infinite loop.
 * @return never
 */
int main(void)
{
    //Reset of all peripherals, Initializes the Flash interface and the Systick
    HAL_Init();

    //Configure the system clock
    SystemClock_Config();

    //Initialize all peripherals
    MX_GPIO_Init();
    MX_DMA_Init();
    MX_ADC_Init();
    MX_DAC_Init();
    MX_I2C1_Init();
    MX_IWDG_Init();
    MX_LPTIM1_Init();
    MX_SPI1_Init();
    MX_COMP1_Init();
    MX_RNG_Init();

    //Enable debug in stop
#ifndef PRJ_DEBUG
#   error "PRJ_DEBUG is not defined!"
#elif PRJ_DEBUG
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_DBGMCU);
    LL_DBGMCU_EnableDBGStopMode();
#endif /*PRJ_DEBUG*/

    //Wakeup to HSI16 from stop mode
    LL_RCC_SetClkAfterWakeFromStop(LL_RCC_STOP_WAKEUPCLOCK_HSI);

    //Initialize app
    app_init();

    //Infinite loop
    app_loop();
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
    RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
    RCC_PeriphCLKInitTypeDef PeriphClkInit = { 0 };

    /** Configure the main internal regulator output voltage
     */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
    HAL_PWR_EnableBkUpAccess();
    /** Initializes the RCC Oscillators according to the specified parameters
     * in the RCC_OscInitTypeDef structure.
     */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_HSI48;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
    /** Initializes the CPU, AHB and APB buses clocks
     */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
    {
        Error_Handler();
    }
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1 | RCC_PERIPHCLK_LPTIM1 | RCC_PERIPHCLK_USB;
    PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
    PeriphClkInit.LptimClockSelection = RCC_LPTIM1CLKSOURCE_LSI;
    PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;

    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
        Error_Handler();
    }

    //Power off MSI
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
    RCC_OscInitStruct.MSIState = RCC_MSI_OFF;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  ADC_ChannelConfTypeDef sConfig = {0};

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  global.hadc.Instance = ADC1;
  global.hadc.Init.OversamplingMode = ENABLE;
  global.hadc.Init.Oversample.Ratio = ADC_OVERSAMPLING_RATIO_16;
  global.hadc.Init.Oversample.RightBitShift = ADC_RIGHTBITSHIFT_NONE;
  global.hadc.Init.Oversample.TriggeredMode = ADC_TRIGGEREDMODE_SINGLE_TRIGGER;
  global.hadc.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV1;
  global.hadc.Init.Resolution = ADC_RESOLUTION_12B;
  global.hadc.Init.SamplingTime = ADC_SAMPLETIME_79CYCLES_5;
  global.hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  global.hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  global.hadc.Init.ContinuousConvMode = ENABLE;
  global.hadc.Init.DiscontinuousConvMode = DISABLE;
  global.hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  global.hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  global.hadc.Init.DMAContinuousRequests = DISABLE;
  global.hadc.Init.EOCSelection = ADC_EOC_SEQ_CONV;
  global.hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  global.hadc.Init.LowPowerAutoWait = ENABLE;
  global.hadc.Init.LowPowerFrequencyMode = DISABLE;
  global.hadc.Init.LowPowerAutoPowerOff = ENABLE;
  if (HAL_ADC_Init(&global.hadc) != HAL_OK)
  {
    Error_Handler();
  }

  //Dummy channel 0 conversion because of ADC issue where first value is wrong
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.Channel = ADC_CHANNEL_0;
  if (HAL_ADC_ConfigChannel(&global.hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfig.Channel = ADC_CHANNEL_3;
  if (HAL_ADC_ConfigChannel(&global.hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfig.Channel = ADC_CHANNEL_4;
  if (HAL_ADC_ConfigChannel(&global.hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfig.Channel = ADC_CHANNEL_7;
  if (HAL_ADC_ConfigChannel(&global.hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfig.Channel = ADC_CHANNEL_VREFINT;
  if (HAL_ADC_ConfigChannel(&global.hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);

}

/**
 * @brief COMP1 Initialization Function
 * @note Modified, do not regenerate!
 */
static void MX_COMP1_Init(void)
{
    LL_GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    LL_IOP_GRP1_EnableClock(LL_IOP_GRP1_PERIPH_GPIOA);
    /**COMP1 GPIO Configuration
     PA3   ------> COMP2_INP (window mode)
     PA4   ------> COMP1_INM
     */
    GPIO_InitStruct.Pin = LL_GPIO_PIN_4;
    GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
    LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    //Comparator
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);   //Enable clock to COMP
    LL_COMP_ConfigInputs(COMP1, LL_COMP_INPUT_MINUS_DAC1_CH1, LL_COMP_WINDOWMODE_COMP1_INPUT_PLUS_COMMON);  //Connect minus input
    LL_COMP_ConfigInputs(COMP2, LL_COMP_INPUT_MINUS_VREFINT, LL_COMP_INPUT_PLUS_IO1);    //Connect plus input
    LL_COMP_SetOutputPolarity(COMP1, LL_COMP_OUTPUTPOL_NONINVERTED);   //Trigger on positive value
    LL_EXTI_EnableRisingTrig_0_31(EXTI_RTSR_RT21);
    LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_21); //Disable EXTI wakeup, it is enabled later
    NVIC_SetPriority(ADC1_COMP_IRQn, 0);
    NVIC_EnableIRQ(ADC1_COMP_IRQn);
    LL_COMP_Disable(COMP1);  //Disable COMP1, it is enabled later
}

/**
 * @brief DAC Initialization Function
 * @param None
 * @retval None
 */
static void MX_DAC_Init(void)
{

    /* USER CODE BEGIN DAC_Init 0 */

    /* USER CODE END DAC_Init 0 */

    DAC_ChannelConfTypeDef sConfig = { 0 };

    /* USER CODE BEGIN DAC_Init 1 */

    /* USER CODE END DAC_Init 1 */
    /** DAC Initialization
     */
    global.hdac.Instance = DAC;
    if (HAL_DAC_Init(&global.hdac) != HAL_OK)
    {
        Error_Handler();
    }
    /** DAC channel OUT1 config
     */
    sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
    sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
    if (HAL_DAC_ConfigChannel(&global.hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
    {
        Error_Handler();
    }
    /* USER CODE BEGIN DAC_Init 2 */

    /* USER CODE END DAC_Init 2 */

}

/**
 * @brief I2C1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_I2C1_Init(void)
{

    /* USER CODE BEGIN I2C1_Init 0 */

    /* USER CODE END I2C1_Init 0 */

    /* USER CODE BEGIN I2C1_Init 1 */

    /* USER CODE END I2C1_Init 1 */
    global.hi2c1.Instance = I2C1;
    global.hi2c1.Init.Timing = 0x00303D5B;
    global.hi2c1.Init.OwnAddress1 = 0;
    global.hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    global.hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    global.hi2c1.Init.OwnAddress2 = 0;
    global.hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    global.hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    global.hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&global.hi2c1) != HAL_OK)
    {
        Error_Handler();
    }
    /** Configure Analogue filter
     */
    if (HAL_I2CEx_ConfigAnalogFilter(&global.hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        Error_Handler();
    }
    /** Configure Digital filter
     */
    if (HAL_I2CEx_ConfigDigitalFilter(&global.hi2c1, 0) != HAL_OK)
    {
        Error_Handler();
    }
    /* USER CODE BEGIN I2C1_Init 2 */

    /* USER CODE END I2C1_Init 2 */

}

/**
 * @brief IWDG Initialization Function
 * @note Manually modified, do not regenerate!
 */
static void MX_IWDG_Init(void)
{
#if 0 /*Watchdog not used*/

#ifndef PRJ_DEBUG
#   error "PRJ_DEBUG is not defined!"
#elif PRJ_DEBUG
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_DBGMCU);
    LL_DBGMCU_APB1_GRP1_FreezePeriph(LL_DBGMCU_APB1_GRP1_IWDG_STOP);
#endif /*PRJ_DEBUG*/

    global.hiwdg.Instance = IWDG;
    global.hiwdg.Init.Prescaler = IWDG_PRESCALER_256;
    global.hiwdg.Init.Window = 4095;
    global.hiwdg.Init.Reload = 4095;
    if (HAL_IWDG_Init(&global.hiwdg) != HAL_OK)
    {
        Error_Handler();
    }
#endif /*0*/
}

/**
 * @brief LPTIM1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_LPTIM1_Init(void)
{
    //Enable LPTIM clock
    LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_LPTIM1);

    //Stop LPTIM on debug stop
#ifndef PRJ_DEBUG
#   error "PRJ_DEBUG is not defined!"
#elif PRJ_DEBUG
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_DBGMCU);
    LL_DBGMCU_APB1_GRP1_FreezePeriph(LL_DBGMCU_APB1_GRP1_LPTIM1_STOP);
#endif /*PRJ_DEBUG*/

    //Enable LPTIM interrupt
    NVIC_SetPriority(LPTIM1_IRQn, 0);
    NVIC_EnableIRQ(LPTIM1_IRQn);

    //Configure LPTIM
    LL_LPTIM_SetClockSource(LPTIM1, LL_LPTIM_CLK_SOURCE_INTERNAL);
    LL_LPTIM_SetPrescaler(LPTIM1, LL_LPTIM_PRESCALER_DIV128);
    LL_LPTIM_SetPolarity(LPTIM1, LL_LPTIM_OUTPUT_POLARITY_REGULAR);
    LL_LPTIM_SetUpdateMode(LPTIM1, LL_LPTIM_UPDATE_MODE_IMMEDIATE);
    LL_LPTIM_SetCounterMode(LPTIM1, LL_LPTIM_COUNTER_MODE_INTERNAL);
    LL_LPTIM_EnableIT_ARRM(LPTIM1);
    LL_LPTIM_EnableIT_CMPM(LPTIM1);
    LL_LPTIM_TrigSw(LPTIM1);

    //Enable LPTIM
    LL_LPTIM_Enable(LPTIM1);

    //Set default period, must be done after enable
    LL_LPTIM_ClearFlag_ARROK(LPTIM1);
    LL_LPTIM_SetAutoReload(LPTIM1, 120 * 38000 / 128);
    uint32_t now = HAL_GetTick();
    while (LL_LPTIM_IsActiveFlag_ARROK(LPTIM1) == 0)
    {
        if (HAL_GetTick() - now > 100)
        {
            Error_Handler();
        }
    }
    LL_LPTIM_ClearFlag_CMPOK(LPTIM1);
    LL_LPTIM_SetCompare(LPTIM1, LL_LPTIM_GetAutoReload(LPTIM1) / 2);    //Half of ARROK as CMP must be < ARR
    now = HAL_GetTick();
    while (LL_LPTIM_IsActiveFlag_CMPOK(LPTIM1) == 0)
    {
        if (HAL_GetTick() - now > 100)
        {
            Error_Handler();
        }
    }

    //Start LPTIM
    LL_LPTIM_StartCounter(LPTIM1, LL_LPTIM_OPERATING_MODE_CONTINUOUS);
}

/**
 * @brief SPI1 Initialization Function
 * @param None
 * @retval None
 */
static void MX_SPI1_Init(void)
{

    /* USER CODE BEGIN SPI1_Init 0 */

    /* USER CODE END SPI1_Init 0 */

    /* USER CODE BEGIN SPI1_Init 1 */

    /* USER CODE END SPI1_Init 1 */
    /* SPI1 parameter configuration*/
    global.hspi1.Instance = SPI1;
    global.hspi1.Init.Mode = SPI_MODE_MASTER;
    global.hspi1.Init.Direction = SPI_DIRECTION_2LINES;
    global.hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
    global.hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
    global.hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
    global.hspi1.Init.NSS = SPI_NSS_SOFT;
    global.hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
    global.hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
    global.hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
    global.hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    global.hspi1.Init.CRCPolynomial = 7;
    if (HAL_SPI_Init(&global.hspi1) != HAL_OK)
    {
        Error_Handler();
    }
    /* USER CODE BEGIN SPI1_Init 2 */

    /* USER CODE END SPI1_Init 2 */

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = { 0 };

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOA, LED_R_Pin | PWR_MODE_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOA, RF_CS_Pin, GPIO_PIN_SET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, VBAT_EN_Pin | RF_SHDN_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pins : PA0 PA1 PA8 PA12 */
    GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_8 | GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pin : DRDY_Pin */
    GPIO_InitStruct.Pin = DRDY_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DRDY_GPIO_Port, &GPIO_InitStruct);

    /*Configure GPIO pins : LED_R_Pin PWR_MODE_Pin RF_CS_Pin */
    GPIO_InitStruct.Pin = LED_R_Pin | PWR_MODE_Pin | RF_CS_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pin : PB0 */
    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : VBAT_EN_Pin RF_SHDN_Pin */
    GPIO_InitStruct.Pin = VBAT_EN_Pin | RF_SHDN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : PA9 PA10 */
    GPIO_InitStruct.Pin = GPIO_PIN_9 | GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF4_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pin : RF_GPIO0_Pin */
    GPIO_InitStruct.Pin = RF_GPIO0_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(RF_GPIO0_GPIO_Port, &GPIO_InitStruct);

    /* EXTI interrupt init*/
    HAL_NVIC_SetPriority(EXTI2_3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI2_3_IRQn);

    HAL_NVIC_SetPriority(EXTI4_15_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);
}

/**
  * @brief RNG Initialization Function
  * @param None
  * @retval None
  */
static void MX_RNG_Init(void)
{
  /* USER CODE BEGIN RNG_Init 0 */
  RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };

  /* USER CODE END RNG_Init 0 */

  /* Peripheral clock enable */
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_RNG);

  /* USER CODE BEGIN RNG_Init 1 */

  /* USER CODE END RNG_Init 1 */
  LL_RNG_Enable(RNG);
  /* USER CODE BEGIN RNG_Init 2 */

  //Initialize random number for CCA
  while(LL_RNG_IsActiveFlag_DRDY(RNG) == 0)
  {
      if (LL_RNG_IsActiveFlag_SEIS(RNG) || LL_RNG_IsActiveFlag_CEIS(RNG))
      {
          Error_Handler();
      }
  }
  global.mers_mlcg = mers_mlcg_srand(LL_RNG_ReadRandData32(RNG));

  //Disable RNG
  LL_RNG_Disable(RNG);

  //Disable HSI48
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_OFF;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
      Error_Handler();
  }

  LL_AHB1_GRP1_DisableClock(LL_AHB1_GRP1_PERIPH_RNG);
  /* USER CODE END RNG_Init 2 */
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
    while(1)
    {
        __BKPT(0);
    }
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
    while (1)
    {
        __BKPT(0);
    }
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
