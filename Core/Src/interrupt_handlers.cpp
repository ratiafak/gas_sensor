/**
 * @file stm32l0xx_it.c
 * @brief Interrupt Service Routines and HAL callbacks.
 * @author Tomas Jakubik
 * @date Sep 10, 2020
 */

#include <stm32l0xx_hal.h>
#include <stm32l0xx_ll_exti.h>
#include <global.hpp>
#include <Sleep.h>
#include <Threshold.h>

extern "C" /*Interrupt handlers without name mangling*/
{

/******************************************************************************/
/*           Cortex-M0+ Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
 * @brief This function handles Non maskable interrupt.
 */
void NMI_Handler(void)
{
    while (1)
    {
        __BKPT(0);
    }
}

/**
 * @brief This function handles Hard fault interrupt.
 */
void HardFault_Handler(void)
{
    while (1)
    {
        __BKPT(0);
    }
}

/**
 * @brief This function handles System service call via SWI instruction.
 */
void SVC_Handler(void)
{
    while (1)
    {
        __BKPT(0);
    }
}

/**
 * @brief This function handles Pendable request for system service.
 */
void PendSV_Handler(void)
{
    while (1)
    {
        __BKPT(0);
    }
}

/**
 * @brief This function handles System tick timer.
 */
void SysTick_Handler(void)
{
    HAL_IncTick();
}

/******************************************************************************/
/* STM32L0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32l0xx.s).                    */
/******************************************************************************/

/**
 * @brief This function handles DMA1 channel 1 interrupt.
 */
void DMA1_Channel1_IRQHandler(void)
{
    /* USER CODE BEGIN DMA1_Channel1_IRQn 0 */

    /* USER CODE END DMA1_Channel1_IRQn 0 */
    HAL_DMA_IRQHandler(&global.hdma_adc);
    /* USER CODE BEGIN DMA1_Channel1_IRQn 1 */

    /* USER CODE END DMA1_Channel1_IRQn 1 */
}

/**
 * @brief ADC conversion complete.
 * @param hadc
 */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
    global.adc.conversionDoneHandler(hadc);  //Finish measurement
}

/**
 * @brief This function handles ADC, COMP1 and COMP2 interrupts (COMP interrupts through EXTI lines 21 and 22).
 */
void ADC1_COMP_IRQHandler(void)
{
    if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_21))
    {
        LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_21);
        Threshold::compInterruptHandler();
        global.gas_threshold = true;
    }
}

/**
 * @brief This function handles LPTIM1 global interrupt / LPTIM1 wake-up interrupt through EXTI line 29.
 */
void LPTIM1_IRQHandler(void)
{
    Sleep::lptimInterruptHandler();
}

/**
 * @brief Interrupt from TIM21 used to calibrate LSI.
 */
void TIM21_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&Sleep::htim21);
}

/**
 * @brief This function handles I2C1 event global interrupt / I2C1 wake-up interrupt through EXTI line 23.
 */
void I2C1_IRQHandler(void)
{
    if (global.hi2c1.Instance->ISR & (I2C_FLAG_BERR | I2C_FLAG_ARLO | I2C_FLAG_OVR))
    {
        HAL_I2C_ER_IRQHandler(&global.hi2c1);
    }
    else
    {
        HAL_I2C_EV_IRQHandler(&global.hi2c1);
    }
}

/**
 * @brief This function handles SPI1 global interrupt.
 */
void SPI1_IRQHandler(void)
{
    HAL_SPI_IRQHandler(&global.hspi1);
}

/**
 * @brief This function handles EXTI line 2 and line 3 interrupts.
 */
void EXTI2_3_IRQHandler(void)
{
    LL_EXTI_ClearFlag_0_31(DRDY_EXTI_Line);
    global.hdc_data_ready.trigger();   //Data ready interrupt
}

uint32_t int_times[64];
uint32_t cnt = 0;

/**
 * @brief This function handles EXTI line 4 to 15 interrupts.
 */
void EXTI4_15_IRQHandler(void)
{
    LL_EXTI_ClearFlag_0_31(RF_GPIO0_EXTI_Line);

    int_times[cnt] = HAL_GetTick();
    cnt = (cnt + 1) & 0x3f;

    global.radio.interruptHandler();    //Configurable radio GPIO interrupt
}

} /*extern "C"*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
