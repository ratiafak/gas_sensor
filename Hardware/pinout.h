/**
 * @file pinout.h
 * @brief Definition of all pins.
 * @author Tomas Jakubik
 * @date Sep 10, 2020
 */


#ifndef PINOUT_H_
#define PINOUT_H_

#define DRDY_Pin                GPIO_PIN_2
#define DRDY_GPIO_Port          GPIOA
#define DRDY_EXTI_Line          LL_EXTI_LINE_2

#define GAS_OUT_Pin             GPIO_PIN_3
#define GAS_OUT_GPIO_Port       GPIOA

#define GAS_THRESHOLD_Pin       GPIO_PIN_4
#define GAS_THRESHOLD_GPIO_Port GPIOA

#define LED_R_Pin               GPIO_PIN_5
#define LED_R_GPIO_Port         GPIOA

#define VGAS_MEAS_Pin           GPIO_PIN_6
#define VGAS_MEAS_GPIO_Port     GPIOA

#define VBAT_MEAS_Pin           GPIO_PIN_7
#define VBAT_MEAS_GPIO_Port     GPIOA

#define VBAT_EN_Pin             GPIO_PIN_1
#define VBAT_EN_GPIO_Port       GPIOB

#define RF_SHDN_Pin             GPIO_PIN_2
#define RF_SHDN_GPIO_Port       GPIOB

#define PWR_MODE_Pin            GPIO_PIN_11
#define PWR_MODE_GPIO_Port      GPIOA

#define RF_CS_Pin               GPIO_PIN_15
#define RF_CS_GPIO_Port         GPIOA

#define RF_GPIO0_Pin            GPIO_PIN_6
#define RF_GPIO0_GPIO_Port      GPIOB
#define RF_GPIO0_EXTI_Line      LL_EXTI_LINE_6

#define BSL_RX_Pin              GPIO_PIN_10
#define BSL_RX_GPIO_Port        GPIOA

#define BSL_TX_Pin              GPIO_PIN_9
#define BSL_TX_GPIO_Port        GPIOA

#define HDC2080_ADDRESS         Hdc2080::ADDRESS_GND

#endif /* PINOUT_H_ */
