/**
 * @file Calibration.cpp
 * @brief Provide calibration values stored in EEPROM.
 * @author Tomas Jakubik
 * @date Oct 5, 2020
 */

#include <Calibration.h>

#include <stm32l0xx_hal.h>


const uint32_t Calibration::VERSION_ADDRESS = DATA_EEPROM_BASE; ///< Location of version
const uint32_t Calibration::DATA_ADDRESS = DATA_EEPROM_BASE + 4;    ///< Location of calibration data
static_assert(Calibration::DATA_ADDRESS + sizeof(Calibration::Data) < DATA_EEPROM_END, "Calibration won't fit into EEPROM.");

/**
 * @brief Write calibration.
 * @param new_data new calibration to write
 */
void Calibration::writeData(const Data &new_data)
{
    HAL_FLASHEx_DATAEEPROM_Unlock();    //Unlock access to EEPROM
    HAL_FLASHEx_DATAEEPROM_Erase (VERSION_ADDRESS);  //Clear version

    for (size_t i = 0; i < ((sizeof(Data) + 3) & ~0x3); i += 4)
    {
        HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, DATA_ADDRESS + i,
                                       reinterpret_cast<const uint32_t*>(&new_data)[i/4]);  //Write calibration
    }

    HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, VERSION_ADDRESS, VERSION);  //Write new valid version
    HAL_FLASHEx_DATAEEPROM_Lock();  //Re-lock access to EEPROM
}




