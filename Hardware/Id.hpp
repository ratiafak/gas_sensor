/**
 * @file Id.hpp
 * @brief Get device unique ID.
 * @author Tomas Jakubik
 * @date Oct 2, 2020
 */


#ifndef ID_HPP_
#define ID_HPP_

#include <cstdint>
#include <cstring>

/**
 * @brief Get device ID.
 */
class Id
{
    struct longId
    {
        uint32_t id[3]; ///< UID of the chip
    };
    uint32_t short_id;  ///< Short handpicked ID

    static const constexpr uint32_t KNOWN_N = 10;
    const longId KNOWN_ID[KNOWN_N] =
    {
        {0x03473835, 0x32333730, 0x00440043},  //1
        {0x03473835, 0x32333730, 0x00320046},  //2
        {0x06473931, 0x37313439, 0x0043001b},  //3
        {0x03473835, 0x32333730, 0x0030005c},  //4
        {0x03473835, 0x32333730, 0x00230029},  //5
        {0x06473931, 0x37313439, 0x0047001f},  //6
        {0x06473931, 0x37313439, 0x002f0017},  //7
        {0x06473931, 0x37313439, 0x00390021},  //8
        {0x03473835, 0x32333730, 0x00270029},  //9
        {0x03473835, 0x32333730, 0x00490057},  //10
    };
public:
    Id()
    {
        longId long_id = getLongId();

        for(uint32_t i = 0; i < KNOWN_N; i++)
        {
            if(std::memcmp(&long_id, &KNOWN_ID[i], sizeof(longId)) == 0)
            {
                short_id = i + 1;
                return;
            }
        }
        short_id = 0;
    }

    uint32_t getShortId() const
    {
        return short_id;
    }

    longId getLongId() const
    {
        longId long_id;
        long_id.id[0] = LL_GetUID_Word0();
        long_id.id[1] = LL_GetUID_Word1();
        long_id.id[2] = LL_GetUID_Word2();
        return long_id;
    }
};

#endif /* ID_HPP_ */
