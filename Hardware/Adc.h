/**
 * @file Adc.h
 * @brief Start measurement and convert values.
 * @author Tomas Jakubik
 * @date Sep 11, 2020
 */

#ifndef ADC_H_
#define ADC_H_

#include <stm32l0xx.h>

#include <cstdint>
#include <Utils.hpp>
#include <FastTimer.h>

class Adc {
    static const constexpr int32_t VBAT_DIVIDER = 11410;  ///< 2^12 * ((10k + 5k6) / 5k6)
    static const constexpr int32_t CHANNELS_N = 6;  ///< Number of channels measured (+1 for the dummy channel 0)
    static const constexpr uint32_t CHANNELS_CHSELR = ADC_CHANNEL_0 | ADC_CHANNEL_3
        | ADC_CHANNEL_4 | ADC_CHANNEL_6 | ADC_CHANNEL_7 | ADC_CHANNEL_VREFINT;  ///< Channel selection
    const int32_t vrefint_cal_3000;   ///< Factory calibration, raw ADC value for internal reference times 3000

    ADC_HandleTypeDef &hadc;  ///< HAL ADC handle

    struct
    {
        uint16_t dummy; ///< Dummy measurement because of ADC issue where first value is wrong
        uint16_t vout;  ///< Output from the gas sensor AFE
        uint16_t vthr;  ///< Threshold for the comparator of gas_out
        uint16_t vgas;  ///< Supply voltage of the gas sensor AFE
        uint16_t vbat;  ///< Battery voltage
        uint16_t vref;  ///< Internal reference
    } raw;   ///< Targets for DMA to write raw ADC values

    FastTimer tm;   ///< Timer for short delays between measurements

    uint32_t calibration; ///< Calibration is not refreshed after reset, needs to be stored

public:
    Task data_ready;    ///< Marks when ADC conversion ends

    /**
     * @brief Init internal variables, do not touch ADC.
     * @param hdac
     */
    Adc(ADC_HandleTypeDef& hadc) :
        vrefint_cal_3000(3000 * *reinterpret_cast<uint16_t*>(0x1FF80078)),
        hadc(hadc),
        raw { 0 }
    {
    }

    /**
     * @brief Nothing to destroy.
     */
    virtual ~Adc()
    {
    }

    /**
     * @brief Do ADC calibration.
     */
    bool calibrate();

    /**
     * @brief Handler called when ADC measurement is done.
     * @param handler_hadc
     * @param success true if conversion done interrupt, false if timeout
     */
    void conversionDoneHandler(ADC_HandleTypeDef *handler_hadc, bool success = true);

    /**
     * @brief Call this when conversion doesn't end in reasonable time.
     */
    void conversionFailed()
    {
        conversionDoneHandler(&hadc, false);
    }

    /**
     * @brief Start ADC measurement.
     * @param vbat_enable true to enable VBAT measurement probe
     * @return true on success
     */
    bool startMeasurement(bool vbat_enable = true);

    /**
     * @brief Convert current value to DAC value to be used as threshold.
     * Static, so it can be used from test.
     * @param raw_vref raw vref AD value [16 bit]
     * @param raw_vgas raw LMP reference pin AD value (16 bit with oversampling)
     * @param current current to WE pin [nA]
     * @param vrefint_cal_3000 factory AD calibration value
     * @param rtia tia resistance [Ohm]
     * @param int_z ratio of voltage on internal zero to LMP reference [%]
     * @return raw DAC value [1/2^16]
     */
    static uint16_t convertCurrentToAd(uint16_t raw_vref,
                                       uint16_t raw_vgas,
                                       int32_t  current,
                                       int32_t  vrefint_cal_3000,
                                       int32_t  rtia,
                                       int32_t  int_z)
    {
        //Round and divide in 64 bit, otherwise it might overflow
        int64_t numerator = ((int64_t)raw_vgas * int_z * (10000 / 5) * vrefint_cal_3000 + (int64_t)current * rtia * (4095 / 5) * raw_vref);
        int64_t denominator = (1000000 / 5) * (int64_t)vrefint_cal_3000;
        int32_t result = numerator / denominator;
        if (result < 0)
        {
            return 0;
        }
        else if (result > UINT16_MAX)
        {
            return UINT16_MAX;
        }
        else
        {
            return result;
        }
    }

    /**
     * @brief Convert AD to current to the WE pin of LMP.
     * Static, so it can be used from test.
     * @param raw_vref raw vref AD value (16 bit with oversampling)
     * @param raw_vout raw LMP output pin AD value (16 bit with oversampling)
     * @param raw_vgas raw LMP reference pin AD value (16 bit with oversampling)
     * @param vrefint_cal_3000 factory AD calibration value
     * @param rtia tia resistance [Ohm]
     * @param int_z ratio of voltage on internal zero to LMP reference [%]
     * @return current to WE pin [nA]
     */
    static int32_t convertAdToCurrent(uint16_t raw_vref,
                                      uint16_t raw_vout,
                                      uint16_t raw_vgas,
                                      int32_t  vrefint_cal_3000,
                                      int32_t  rtia,
                                      int32_t  int_z)
    {
        //Round and divide in 64 bit, otherwise it might overflow
        int64_t numerator = (int64_t)(10000/5) * vrefint_cal_3000 * (100 * raw_vout - raw_vgas * int_z);
        int64_t denominator = ((int64_t)rtia * (4095/5) * raw_vref);
        if (numerator >= 0)
        {
            return (numerator + (denominator/2)) / denominator;
        }
        else
        {
            return (numerator - (denominator/2)) / denominator; //Negative division rounds towards zero
        }
    }

    /**
     * @brief Get VREF voltage on the MCU.
     * @return voltage [mV]
     */
    int32_t getVref();

    /**
     * @brief Convert current value to DAC value to be used as threshold.
     * @param current current to WE pin [nA]
     * @param rtia tia resistance [Ohm]
     * @param int_z voltage of internal zero as a ratio of LMP reference [%]
     * @return raw DAC value [1/2^16]
     */
    uint16_t convertThreshold(int32_t current, int32_t tia, int32_t int_z);

    /**
     * @brief Get current to the WE pin of LMP.
     * @param rtia TIA gain resistor value [Ohm]
     * @param int_z voltage of internal zero as a ratio of LMP reference [%]
     * @return current [nA]
     */
    int32_t getGasCurrent(int32_t tia, int32_t int_z);

    /**
     * @brief Get value on the GAS_THRESHOLD in a form of a current to the WE pin of LMP.
     * @param rtia TIA gain resistor value [Ohm]
     * @param int_z voltage of internal zero as a ratio of LMP reference [%]
     * @return current [nA]
     */
    int32_t getGasThreshold(int32_t tia, int32_t int_z);

    /**
     * @brief Get VBAT voltage on batteries.
     * @return voltage [mV]
     */
    int32_t getVbat();
};

#endif /* ADC_H_ */
