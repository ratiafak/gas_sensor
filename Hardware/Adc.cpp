/**
 * @file Adc.cpp
 * @brief Start measurement and convert values.
 * @author Tomas Jakubik
 * @date Sep 11, 2020
 */

#include <Adc.h>
#include <pinout.h>
#include <stm32l0xx_ll_gpio.h>
#include <stm32l0xx_ll_bus.h>
#include <stm32l0xx_ll_adc.h>
#include <stm32l0xx_ll_system.h>

/**
 * @brief Handler called when ADC measurement is done.
 * @param handler_hadc
 * @param success true if conversion done interrupt, false if timeout
 */
void Adc::conversionDoneHandler(ADC_HandleTypeDef *handler_hadc, bool success)
{
    LL_GPIO_ResetOutputPin(VBAT_EN_GPIO_Port, VBAT_EN_Pin);  //Disable VBAT measurement circuit
    LL_GPIO_ResetOutputPin(PWR_MODE_GPIO_Port, PWR_MODE_Pin);  //Resume power source function
    tm.deinit();
    LL_APB2_GRP1_ForceReset(LL_APB2_GRP1_PERIPH_ADC1);  //Reset ADC after use to lower consumption
    handler_hadc->State = HAL_ADC_STATE_RESET;

    if (success)
    {
        data_ready.trigger();   //Notify app about results
    }
}

/**
 * @brief Do ADC calibration.
 */
bool Adc::calibrate()
{
    bool ret = false;

    //Reinit ADC
    LL_APB2_GRP1_ReleaseReset(LL_APB2_GRP1_PERIPH_ADC1);
    if (HAL_ADC_Init(&hadc) != HAL_OK)
    {
        return false;
    }

    //Switch power source to the PWM mode for the calibration
    LL_GPIO_SetOutputPin(PWR_MODE_GPIO_Port, PWR_MODE_Pin);
    tm.delay(50);

    if (HAL_ADCEx_Calibration_Start(&hadc, ADC_SINGLE_ENDED) == HAL_OK)
    {
        //Calibration needs to be done twice because first value of ADC is shit
        if (HAL_ADCEx_Calibration_Start(&hadc, ADC_SINGLE_ENDED) == HAL_OK)
        {
            calibration = HAL_ADCEx_Calibration_GetValue(&hadc, ADC_SINGLE_ENDED);
            ret = true;  //Success
        }
    }

    LL_GPIO_ResetOutputPin(PWR_MODE_GPIO_Port, PWR_MODE_Pin);  //Resume power source function
    tm.deinit();
    LL_APB2_GRP1_ForceReset(LL_APB2_GRP1_PERIPH_ADC1);  //Reset ADC after use to lower consumption
    hadc.State = HAL_ADC_STATE_RESET;
    return ret;
}

/**
 * @brief Start ADC measurement.
 * @param vbat_enable true to enable VBAT measurement probe
 * @return true on success
 */
bool Adc::startMeasurement(bool vbat_enable)
{
    //Reinit ADC
    LL_APB2_GRP1_ReleaseReset(LL_APB2_GRP1_PERIPH_ADC1);
    if (HAL_ADC_Init(&hadc) != HAL_OK)
    {
        return false;
    }

    ///@note HAL_ADC_ConfigChannel() can only write one channel at a time.
    hadc.Instance->CHSELR = CHANNELS_CHSELR;

    LL_ADC_SetCommonPathInternalCh(ADC, LL_ADC_PATH_INTERNAL_VREFINT);

    ///@note HAL_ADCEx_Calibration_SetValue() cannot be used.
    /// It requires enabled but not working ADC which cannot happen.
    hadc.Instance->CALFACT = calibration;

    //Enable VBAT measurement circuit
    if (vbat_enable)
    {
        LL_GPIO_SetOutputPin(VBAT_EN_GPIO_Port, VBAT_EN_Pin);
    }

    //Switch power source to the PWM mode for the measurement
    LL_GPIO_SetOutputPin(PWR_MODE_GPIO_Port, PWR_MODE_Pin);
    tm.delay(50);

    //Measure
    if (HAL_ADC_Start_DMA(&hadc, reinterpret_cast<uint32_t*>(&raw), CHANNELS_N) == HAL_OK)
    {
        return true;
    }

    //ADC failed
    LL_GPIO_ResetOutputPin(PWR_MODE_GPIO_Port, PWR_MODE_Pin);   //Resume powersource
    LL_GPIO_ResetOutputPin(VBAT_EN_GPIO_Port, VBAT_EN_Pin);   //Disable VBAT measurement circuit
    tm.deinit();
    LL_APB2_GRP1_ForceReset(LL_APB2_GRP1_PERIPH_ADC1);  //Reset ADC after use to lower consumption
    hadc.State = HAL_ADC_STATE_RESET;
    return false;
}

/**
 * @brief Get VREF voltage on the MCU.
 * @return voltage [mV]
 */
int32_t Adc::getVref()
{
    return (vrefint_cal_3000 << 4) / raw.vref;
}

/**
 * @brief Convert current value to DAC value to be used as threshold.
 * @param current current to WE pin [nA]
 * @param rtia tia resistance [Ohm]
 * @param int_z voltage of internal zero as a ratio of LMP reference [%]
 * @return raw DAC value [1/2^16]
 */
uint16_t Adc::convertThreshold(int32_t current, int32_t rtia, int32_t int_z)
{
    return convertCurrentToAd(raw.vref, raw.vgas, current, vrefint_cal_3000, rtia, int_z);
}

/**
 * @brief Get current to the WE pin of LMP.
 * @param rtia TIA gain resistor value [Ohm]
 * @param int_z voltage of internal zero as a ratio of LMP reference [%]
 * @return current [nA]
 */
int32_t Adc::getGasCurrent(int32_t rtia, int32_t int_z)
{
    return convertAdToCurrent(raw.vref, raw.vout, raw.vgas, vrefint_cal_3000, rtia, int_z);
}

/**
 * @brief Get value on the GAS_THRESHOLD in a form of a current to the WE pin of LMP.
 * @param rtia TIA gain resistor value [Ohm]
 * @param int_z voltage of internal zero as a ratio of LMP reference [%]
 * @return current [nA]
 */
int32_t Adc::getGasThreshold(int32_t rtia, int32_t int_z)
{
    return convertAdToCurrent(raw.vref, raw.vthr, raw.vgas, vrefint_cal_3000, rtia, int_z);
}

/**
 * @brief Get VBAT voltage on batteries.
 * @return voltage [mV]
 */
int32_t Adc::getVbat()
{
    return ((((static_cast<int32_t>(raw.vbat) * getVref()) >> 16) * VBAT_DIVIDER) >> 12);
}


