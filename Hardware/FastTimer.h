/**
 * @file FastTimer.h
 * @brief Fast timer for less-than-ms timing.
 * @author Tomas Jakubik
 * @date Jul 1, 2021
 */

#ifndef FASTTIMER_H_
#define FASTTIMER_H_

#include <cstdint>

/**
 * @brief For less-than-ms timing.
 * Inited on creation and deinited on destruction.
 * Use delay() to wait.
 * No interrupt.
 * Do not use to wait for longer than ms, because it could miss the time and wait 65 ms more.
 */
class FastTimer
{
    static int count;   ///< Count how many times the timer is used
    bool used;          ///< True if timer is used by this instance

public:
    FastTimer() : used(false) {}

    /**
     * @brief Init timer.
     * @return true if hardware was initialized which took some time
     */
    bool init();

    /**
     * @brief Wait for a brief time.
     * @param us time to wait [us]
     */
    void delay(uint16_t us);

    /**
     * @brief Deinit timer.
     * Optionally deinit before destroying.
     */
    void deinit();

    /**
     * @brief Deinit in destructor as well.
     */
    virtual ~FastTimer()
    {
        deinit();
    }
};

#endif /* FASTTIMER_H_ */
