/**
 * @file Lmp91000.cpp
 * @brief Control LMP91000 Analog Frontend.
 * @author Tomas Jakubik
 * @date Sep 10, 2020
 */

#include <Lmp91000.h>

/**
 * @brief Set AFE state.
 * @param new_mode requested state
 * @return true on success
 */
bool Lmp91000::setMode(Mode new_mode)
{
    uint8_t mode_reg;

    //Check valid mode
    switch(new_mode)
    {
        case Mode::Unknown:
            return false;

        case Mode::Sleep:
            mode_reg = 0x0;  //Deep sleep
            break;

        case Mode::On2Lead:
            mode_reg = 0x1;  //2-lead ground referred galvanic cell
            break;

        case Mode::Standby:
            mode_reg = 0x2;  //Cell powered, TIA off
            break;

        case Mode::On3Lead:
            mode_reg = 0x3;  //3-lead amperometric cell
            break;

        default:
            return false;
    }

    //Wait for AFE to be ready
    if (waitUntilReady() == false)
    {
        return false;
    }

    //Set requested mode
    if(writeReg(MODECN, mode_reg) == false)
    {
        mode = Mode::Unknown;
        return false;
    }

    mode = new_mode;
    return true;
}

/**
 * @brief Configure the AFE.
 * @param tia_gain TIA feedback resistance selection
 * @param r_load load resistance selection
 * @param ref_source_external true for VREF as source, false for VDD as source
 * @param int_z internal zero selection (Percentage of the source reference)
 * @param bias_positive true for positive bias (V_WE - V_RE) > 0V, false for negative bias (V_WE - V_RE) < 0V
 * @param bias BIAS selection (Percentage of the source reference)
 * @return true on success
 */
bool Lmp91000::configure(TiaGain tia_gain, RLoad r_load, bool ref_source_external, IntZ int_z, bool bias_positive, Bias bias)
{
    if((tia_gain > TiaGain::r350000)
        || (r_load > RLoad::r100)
        || (int_z > IntZ::bypass)
        || (bias > Bias::p24))
    {
        return false;   //Invalid config
    }

    //Wait for AFE to be aready
    if (waitUntilReady() == false)
    {
        return false;
    }

    //Try config
    do
    {
        //Power off the cell
        if(writeReg(MODECN, 0x00) == false)
        {
            break;
        }

        //Unlock config access
        if(writeReg(LOCK, 0x00) == false)
        {
            break;
        }

        //Write TIA config
        if (writeReg(TIACN, (static_cast<uint8_t>(tia_gain) << 2) | static_cast<uint8_t>(r_load)) == false)
        {
            break;
        }

        //Write reference config
        if (writeReg(REFCN, (ref_source_external ? (1 << 7) : 0) | (static_cast<uint8_t>(int_z) << 5)
                     | (bias_positive ? (1 << 4) : 0) | static_cast<uint8_t>(bias)) == false)
        {
            break;
        }

        //Lock config access
        if(writeReg(LOCK, 0x01) == false)
        {
            break;
        }

        //Return to operation mode
        if(mode == Mode::Unknown)
        {
            mode = Mode::Sleep;
        }
        if(setMode(mode) == false)
        {
            break;
        }

        return true;
    } while(0);

    //Config failed
    mode = Mode::Unknown;   //Error, unknown state
    writeReg(MODECN, 0x00);  //Try to power off the cell
    return false;
}

/**
 * @brief Get tia resistance in Ohm.
 * @return resistance [Ohm]
 */
int32_t Lmp91000::tia_ohm(TiaGain tia_gain)
{
    switch(tia_gain)
    {
        case TiaGain::r2750   : return   2750;
        case TiaGain::r3500   : return   3500;
        case TiaGain::r7000   : return   7000;
        case TiaGain::r14000  : return  14000;
        case TiaGain::r35000  : return  35000;
        case TiaGain::r120000 : return 120000;
        case TiaGain::r350000 : return 350000;
        default: return 0;
    }
}

/**
 * @brief Get internal zero ratio.
 * @param int_z internal zero LMP config
 * @return voltage of internal zero as a ratio of LMP reference [%]
 */
int32_t Lmp91000::int_z_coeff(IntZ int_z)
{
    switch(int_z)
    {
        case IntZ::bypass: return 100;
        case IntZ::p20   : return  20;
        case IntZ::p50   : return  50;
        case IntZ::p67   : return  67;
        default: return 0;
    }
}

/**
 * @brief Write one register.
 * @param address register address
 * @param value register value
 * @return true on success
 */
bool Lmp91000::writeReg(uint8_t address, uint8_t value)
{
    return (HAL_I2C_Mem_Write(&hi2c, DEVICE_ADDRESS, address, 1, &value, 1, i2c_timeout) == HAL_OK);
}

/**
 * @brief Wait until AFE is ready.
 * @return true on success
 */
bool Lmp91000::waitUntilReady()
{
    uint32_t attempts = ready_attempts;
    uint8_t reg;

    do
    {
        if(attempts-- == 0)
        {
            return false;   //Limit number of tries
        }

        //Read status register
        if (HAL_I2C_Mem_Read(&hi2c, DEVICE_ADDRESS, STATUS, 1, &reg, 1, i2c_timeout) != HAL_OK)
        {
            return false;   //Couldn't read
        }
    } while(reg != 0x01);   //Must be read as 0x01 when ready

    return true;
}
