/**
 * @file Lmp91000.h
 * @brief Control LMP91000 Analog Frontend.
 * @author Tomas Jakubik
 * @date Sep 10, 2020
 */

#ifndef LMP91000_H_
#define LMP91000_H_

#include <stm32l0xx.h>
#include <stm32l0xx_hal_i2c.h>
#include <cstdint>
#include <cmath>

class Lmp91000
{
public:
    ///Mode of the AFE
    enum class Mode
    {
        Unknown,    ///< Unknown state
        Sleep,      ///< Deep sleep
        On2Lead,    ///< 2-lead ground referred galvanic cell
        Standby,    ///< Cell powered, TIA off
        On3Lead,    ///< 3-lead amperometric cell
    };

    ///Bias as percentage of source reference
    enum class Bias : uint8_t
    {
        p00 = 0x0, ///<  0%
        p01 = 0x1, ///<  1%
        p02 = 0x2, ///<  2%
        p04 = 0x3, ///<  4%
        p06 = 0x4, ///<  6%
        p08 = 0x5, ///<  8%
        p10 = 0x6, ///< 10%
        p12 = 0x7, ///< 12%
        p14 = 0x8, ///< 14%
        p16 = 0x9, ///< 16%
        p18 = 0xa, ///< 18%
        p20 = 0xb, ///< 20%
        p22 = 0xc, ///< 22%
        p24 = 0xd, ///< 24%
    };

    ///Internal zero selection as percentage of source reference
    enum class IntZ : uint8_t
    {
        p20    = 0x0, ///< 20%
        p50    = 0x1, ///< 50%
        p67    = 0x2, ///< 67%
        bypass = 0x3, ///< Internal zero circuitry bypassed (only in O2 ground referred measurement)
    };

    ///R Load selection
    enum class RLoad : uint8_t
    {
        r10  = 0x0, ///<  10 Ohm
        r33  = 0x1, ///<  33 Ohm
        r50  = 0x2, ///<  50 Ohm
        r100 = 0x3, ///< 100 Ohm
    };

    ///TIA feedback resistance selection
    enum class TiaGain : uint8_t
    {
        external = 0x0, ///< External resistance
        r2750    = 0x1, ///<   2.75 kOhm
        r3500    = 0x2, ///<   3.5  kOhm
        r7000    = 0x3, ///<   7    kOhm
        r14000   = 0x4, ///<  14    kOhm
        r35000   = 0x5, ///<  35    kOhm
        r120000  = 0x6, ///< 120    kOhm
        r350000  = 0x7, ///< 350    kOhm
    };

private:
    static const constexpr uint8_t STATUS = 0x00; ///< Status Register
    static const constexpr uint8_t LOCK   = 0x01; ///< Protection Register
    static const constexpr uint8_t TIACN  = 0x10; ///< TIA Control Register
    static const constexpr uint8_t REFCN  = 0x11; ///< Reference Control Register
    static const constexpr uint8_t MODECN = 0x12; ///< Mode Control Register

    static const constexpr uint16_t DEVICE_ADDRESS = 0x90; ///< LMP91000 I2C address

    const uint32_t i2c_timeout; ///< Timeout for I2C transfer
    const uint32_t ready_attempts; ///< Number of attempts to read AFE ready status

    I2C_HandleTypeDef &hi2c;    ///< HAL I2C handle
    Mode mode;  ///< Current AFE state

public:
    /**
     * @brief Configure library, do not touch I2C.
     * @param hi2c already initialized HAL I2C handle
     * @param i2c_timeout timeout for I2C transfer [HAL ticks]
     * @param ready_attempts number of attempts to read AFE ready status
     */
    Lmp91000(I2C_HandleTypeDef &hi2c, uint32_t i2c_timeout = 100, uint32_t ready_attempts = 4) :
        i2c_timeout(i2c_timeout),
        ready_attempts(ready_attempts),
        hi2c(hi2c),
        mode(Mode::Unknown)
    {
    }

    /**
     * @brief Nothing to destroy.
     */
    virtual ~Lmp91000()
    {
    }

    /**
     * @brief Set AFE state.
     * @param new_mode requested state
     * @return true on success
     */
    bool setMode(Mode new_mode);

    /**
     * @brief Get current LMP mode.
     * @return current mode
     */
    Mode getMode() const
    {
        return mode;
    }

    /**
     * @brief Configure the AFE.
     * @param tia_gain TIA feedback resistance selection
     * @param r_load load resistance selection
     * @param ref_source_external true for VREF as source, false for VDD as source
     * @param int_z internal zero selection (Percentage of the source reference)
     * @param bias_positive true for positive bias (V_WE - V_RE) > 0V, false for negative bias (V_WE - V_RE) < 0V
     * @param bias BIAS selection (Percentage of the source reference)
     * @return true on success
     */
    bool configure(TiaGain tia_gain, RLoad r_load, bool ref_source_external, IntZ int_z, bool bias_positive, Bias bias);
    
    /**
     * @brief Get tia resistance in Ohm.
     * @return resistance [Ohm]
     */
    static int32_t tia_ohm(TiaGain tia_gain);

    /**
     * @brief Get internal zero ratio.
     * @param int_z internal zero LMP config
     * @return voltage of internal zero as a ratio of LMP reference [%]
     */
    static int32_t int_z_coeff(IntZ int_z);

private:
    /**
     * @brief Write one register.
     * @param address register address
     * @param value register value
     * @return true on success
     */
    bool writeReg(uint8_t address, uint8_t value);

    /**
     * @brief Wait until AFE is ready.
     * @return true on success
     */
    bool waitUntilReady();
};

#endif /* LMP91000_H_ */
