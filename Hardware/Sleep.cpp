/**
 * @file Sleep.cpp
 * @brief Low power and LPTIM timer.
 * @author Tomas Jakubik
 * @date Dec 11, 2020
 */

#include <Sleep.h>
#include <Utils.hpp>
#include <stm32l0xx_ll_lptim.h>
#include <stm32l0xx_ll_bus.h>
#include <stm32l0xx_ll_pwr.h>
#include <stm32l0xx_ll_rcc.h>
#include <stm32l0xx_ll_system.h>
#include <stm32l0xx_ll_gpio.h>
#include <pinout.h>

TIM_HandleTypeDef Sleep::htim21;   ///< HAL handle for TIM21, used for LSI calibration

volatile Sleep::LsiCalibrationStage Sleep::lsiStage = Sleep::LsiCalibrationStage::Off;   ///< Current stage when calibrating LSI
volatile uint16_t Sleep::lsiCapture1 = 0;   ///< Timer capture of the first LSI edge
volatile uint32_t Sleep::lsiFrequency = 38000;  ///< LSI frequency

volatile uint32_t Sleep::lpTimeMinutes = 0; ///< Minute overloads of low-power time

volatile Sleep::PlannedInterrupt Sleep::plannedInterrupt = {{0, 0}, false};  ///< Remember interrupt time if it is in a future minute

/**
  * @brief  HAL interrupt callback.
  * @param  htim TIM HAL handle
*/
extern "C" void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
  Sleep::lsiTimCapture(HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1)); //Get the Input Capture value
}

/**
 * @brief Store one capture of LSI signal.
 * @param capture TIM value captured for LSI edge
 */
void Sleep::lsiTimCapture(uint16_t capture)
{
    if (lsiStage == LsiCalibrationStage::Edge1) //Capture first edge of LSI signal
    {
        lsiCapture1 = capture;
        lsiStage = LsiCalibrationStage::Edge2;
    }
    else if (lsiStage == LsiCalibrationStage::Edge2)    //Capture second edge of LSI signal
    {
        lsiFrequency = (8 * SystemCoreClock) / (capture - lsiCapture1);
        lsiStage = LsiCalibrationStage::Finish;
    }
}

/**
 * @brief Set CMP value of LPTIM.
 * @param cmp new CMP value
 * @return true on success
 */
bool Sleep::setCompare(uint16_t cmp)
{
    uint32_t now = HAL_GetTick();
    while (LL_LPTIM_IsActiveFlag_CMPOK(LPTIM1) == 0)    //Wait for CMPOK and for previous CMP value to be used
    {
        if (HAL_GetTick() - now > 10)
        {
            return false;
        }
        __WFI();
    }

    LL_LPTIM_ClearFlag_CMPOK(LPTIM1);
    LL_LPTIM_SetCompare(LPTIM1, cmp);

    return true;
}

/**
 * @brief Configures TIM21 to measure the LSI oscillator frequency and sets LPTIM accordingly.
 * @return LSI frequency, zero on error
 */
uint32_t Sleep::calibrateLsi(void)
{
    TIM_IC_InitTypeDef TIMInput_Config;

    //Enable TIM21 clock
    LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM21);

    //Set TIMx instance
    htim21.Instance = TIM21;

    //TIM21 configuration: Input Capture mode
    //The LSI oscillator is connected to TIM21 TIM_CHANNEL_1.
    //The Rising edge is used as active edge.
    //The TIM21 Capture/Compare register associated to TIM_CHANNEL_1 is used to compute the frequency value.
    htim21.Init.Prescaler = 0;
    htim21.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim21.Init.Period = 0xFFFF;
    htim21.Init.ClockDivision = 0;
    if (HAL_TIM_IC_Init(&htim21) != HAL_OK)
    {
        return 0;
    }

    //Connect internally the TIM21 Input Capture of TIM_CHANNEL_1 to the LSI clock output
    HAL_TIMEx_RemapConfig(&htim21, TIM21_TI1_LSI);

    //Configure the Input Capture of TIM_CHANNEL_1
    TIMInput_Config.ICPolarity = TIM_ICPOLARITY_RISING;
    TIMInput_Config.ICSelection = TIM_ICSELECTION_DIRECTTI;
    TIMInput_Config.ICPrescaler = TIM_ICPSC_DIV8;
    TIMInput_Config.ICFilter = 0;
    if (HAL_TIM_IC_ConfigChannel(&htim21, &TIMInput_Config, TIM_CHANNEL_1) != HAL_OK)
    {
        return 0;
    }

    //Enable TIM interrupt
    NVIC_SetPriority(TIM21_IRQn, 0);
    NVIC_EnableIRQ(TIM21_IRQn);

    //Start the TIM Input Capture measurement in interrupt mode
    lsiStage = LsiCalibrationStage::Edge1;
    if (HAL_TIM_IC_Start_IT(&htim21, TIM_CHANNEL_1) != HAL_OK)
    {
        return 0;
    }

    //Wait until the TIM21 gets 2 LSI edges
    uint32_t timeout = HAL_GetTick();
    while (lsiStage != LsiCalibrationStage::Finish)
    {
        if ((HAL_GetTick() - timeout) > 10)    //Timeout
        {
            return 0;
        }
    }
    lsiStage = LsiCalibrationStage::Off;

    //Disable TIM21 Capture/Compare channel Interrupt Request
    HAL_TIM_IC_Stop_IT(&htim21, TIM_CHANNEL_1);

    //Deinitialize the TIM21 peripheral registers to their default reset values
    HAL_TIM_IC_DeInit(&htim21);

    //Set LPTIM reload to a minute
    LL_LPTIM_ClearFlag_CMPOK(LPTIM1);
    LL_LPTIM_SetCompare(LPTIM1, 0); //Reset CMP as it has to be always < ARR
    LL_LPTIM_ClearFlag_ARROK(LPTIM1);
    LL_LPTIM_SetAutoReload(LPTIM1, 60 * lsiFrequency / 128);    //Reload after a minute
    uint32_t now = HAL_GetTick();
    while (LL_LPTIM_IsActiveFlag_ARROK(LPTIM1) == 0)
    {
        if (HAL_GetTick() - now > 10)
        {
            return 0;
        }
        __WFI();
    }

    return lsiFrequency;
}

/**
 * @brief Get low-power time.
 * @return low power time from start
 */
Sleep::LpTime Sleep::getLpTime()
{
    uint32_t submin1, submin2, period, arrm;
    LpTime now = LpTime(0, 0);

    {
        Critical critical;

        period = LL_LPTIM_GetAutoReload(LPTIM1) + 1;
        now.minute = lpTimeMinutes;
        submin1 = LL_LPTIM_GetCounter(LPTIM1);
        arrm = LL_LPTIM_IsActiveFlag_ARRM(LPTIM1);
        submin2 = LL_LPTIM_GetCounter(LPTIM1);
    }

    if (((submin1 > (period / 2)) && (submin2 < (period / 2)))
        || (arrm != 0))  //Overflow happened while reading
    {
        now.minute++;
    }

    now.subsecond = (SUBSECONDS_N*60) * submin2 / period;
    return now;
}

/**
 * @brief Sleep until next interrupt.
 */
void Sleep::sleep()
{
    Critical critical;

    //Pause systick interrupts
    HAL_SuspendTick();

    //Stop
    HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
    LL_PWR_SetRegulModeLP(LL_PWR_REGU_LPMODES_MAIN);    //Return voltage regulator to normal mode

    //Resume Systick interrupts
    HAL_ResumeTick();
}

/**
 * @brief Set interrupt to a given time.
 * @param interrupt time of interrupt
 */
bool Sleep::setInterrupt(LpTime interrupt)
{
    interrupt.sanitize();
    if (interrupt.subsecond == 0)
    {
        setCompare(0);
        return true;   //Whole minute interrupts are always on
    }

    LpTime now = getLpTime();  //Get current time

    if (now >= interrupt) //Not in the future
    {
        return false;
    }

    if (now.minute == interrupt.minute)   //Interrupt this minute
    {
        //+1 so interrupt is after flip of a second
        setCompare(interrupt.subsecond * LL_LPTIM_GetAutoReload(LPTIM1) / (SUBSECONDS_N*60) + 1);
        plannedInterrupt.active = false;
    }
    else    //Store as planned interrupt
    {
        plannedInterrupt.at.minute = interrupt.minute;
        plannedInterrupt.at.subsecond = interrupt.subsecond;
        plannedInterrupt.active = true;
    }

    return true;
}

/**
 * @brief Interrupt handler for LPTIM.
 */
void Sleep::lptimInterruptHandler()
{
    //Compare interrupt
    if (LL_LPTIM_IsActiveFlag_CMPM(LPTIM1))
    {
        LL_LPTIM_ClearFLAG_CMPM(LPTIM1);

        //Just wakeup
    }

    //Reload interrupt
    if (LL_LPTIM_IsActiveFlag_ARRM(LPTIM1))
    {
        LL_LPTIM_ClearFLAG_ARRM(LPTIM1);
        lpTimeMinutes++;    //Count minutes

        if (plannedInterrupt.active && (lpTimeMinutes == plannedInterrupt.at.minute))
        {
            //Apply interrupt which is this minute
            //+1 so interrupt is after flip of a second
            setCompare(plannedInterrupt.at.subsecond * LL_LPTIM_GetAutoReload(LPTIM1) / (SUBSECONDS_N*60) + 1);
            plannedInterrupt.active = false;
        }
    }
}

