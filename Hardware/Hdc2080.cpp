/**
 * @file Hdc2080.cpp
 * @brief Class to control HDC2080.
 * @author Tomas Jakubik
 * @date Sep 9, 2020
 */

#include "Hdc2080.h"


/**
 * @brief Init the sensor and put to sleep.
 * @return true on success
 */
bool Hdc2080::init()
{
    uint8_t regs[4];
    int32_t attempts;

    inited = false;
    measurement_in_progress = false;

    //Assuming 4 registers in continuous memory
    static_assert(MANUFACTURER_ID_HIGH == (MANUFACTURER_ID_LOW+1), "Reading IDs together!");
    static_assert(DEVICE_ID_LOW        == (MANUFACTURER_ID_LOW+2), "Reading IDs together!");
    static_assert(DEVICE_ID_HIGH       == (MANUFACTURER_ID_LOW+3), "Reading IDs together!");

    //Read IDs
    if (HAL_I2C_Mem_Read(&hi2c, device_address, MANUFACTURER_ID_LOW, 1, regs, 4, i2c_timeout) != HAL_OK)
    {
        return false;   //Couldn't read
    }

    //Check IDs
    if ((MANUFACTURER_ID_VAL != (regs[0] | (regs[1] << 8)))
        || (DEVICE_ID_VAL != (regs[2] | (regs[3] << 8))))
    {
        return false;   //ID doesn't fit
    }

    //Soft reset
    if (writeReg(RESET_DRDY_INT_CONF, 0x80) == false)
    {
        return false;
    }

    attempts = ready_attempts;
    do
    {
        if(attempts-- == 0)
        {
            return false;   //Limit number of tries
        }

        //Read reset register
        if (HAL_I2C_Mem_Read(&hi2c, device_address, RESET_DRDY_INT_CONF, 1, regs, 1, i2c_timeout) != HAL_OK)
        {
            return false;   //Couldn't read
        }
    } while(regs[0] != 0x00);   //Must be read as 0 after reset

    //Enable data ready interrupt
    if (writeReg(INTERRUPT_ENABLE, 0x80) == false)
    {
        return false;
    }

    //Enable interrupt pin, measurement on request
    if (writeReg(RESET_DRDY_INT_CONF, 0x06) == false)
    {
        return false;
    }

    //Trigger a measurement with requested resolution
    if (writeReg(MEASUREMENT_CONFIGURATION, precision | 0x1) == false)
    {
        return false;
    }

    measurement_in_progress = true;
    inited = true;
    return true;
}

/**
 * @brief Trigger a measurement.
 * @return true on success
 */
bool Hdc2080::measure()
{
    if (measurement_in_progress || (inited == false))
    {
        return false;   //Measurement is already in progress or not inited
    }

    //Trigger a measurement with requested resolution
    if (writeReg(MEASUREMENT_CONFIGURATION, precision | 0x1) == false)
    {
        return false;
    }

    measurement_in_progress = true;
    return true;
}

/**
 * @brief Readout measured values.
 * Call this after DRDY interrupt.
 * @return true on success
 */
bool Hdc2080::readout()
{
    uint8_t results[4];

    if (inited == false)
    {
        return false;
    }

    //Assuming 4 registers in continuous memory
    static_assert(TEMPERATURE_HIGH == (TEMPERATURE_LOW+1), "Reading temperature and humidity together!");
    static_assert(HUMIDITY_LOW     == (TEMPERATURE_LOW+2), "Reading temperature and humidity together!");
    static_assert(HUMIDITY_HIGH    == (TEMPERATURE_LOW+3), "Reading temperature and humidity together!");

    //Read values
    if (HAL_I2C_Mem_Read(&hi2c, device_address, TEMPERATURE_LOW, 1, results, 4, i2c_timeout) != HAL_OK)
    {
        inited = false;
        return false;   //Couldn't read
    }

    temperature = results[0] | (results[1] << 8);
    humidity = results[2] | (results[3] << 8);
    measurement_in_progress = false;

    return true;
}

/**
 * @brief Get temperature in full resolution.
 * @return temperature [deg C], NAN if temperature is not valid
 */
float Hdc2080::getTemperature()
{
    if ((inited == false) || measurement_in_progress)
    {
        return NAN;
    }

    return ((165.0 * temperature) / (1 << 16)) - 40.0;
}

/**
 * @brief Get humidity in full resolution.
 * @return humidity [% RH], NAN if humidity is not valid
 */
float Hdc2080::getHumidity()
{
    if ((inited == false) || measurement_in_progress)
    {
        return NAN;
    }

    return (100.0 * humidity) / (1 << 16);
}

/**
 * Get temperature with 0.1 degree resolution.
 * @return temperature [0.1 deg C], INT32_MIN if temperature is not valid
 */
int32_t Hdc2080::getTemperature0C1()
{
    if ((inited == false) || measurement_in_progress)
    {
        return INT32_MIN;
    }

    return ((1650 * static_cast<int32_t>(temperature)) >> 16) - 400;
}

/**
 * Get humidity with 1 % resolution.
 * @return humidity [% RH], INT32_MIN if humidity is not valid
 */
int32_t Hdc2080::getHumidityPercent()
{
    if ((inited == false) || measurement_in_progress)
    {
        return INT32_MIN;
    }

    return (100 * static_cast<int32_t>(humidity)) >> 16;
}

/**
 * @brief Write one register.
 * @param address register address
 * @param value register value
 * @return true on success
 */
bool Hdc2080::writeReg(uint8_t address, uint8_t value)
{
    return (HAL_I2C_Mem_Write(&hi2c, device_address, address, 1, &value, 1, i2c_timeout) == HAL_OK);
}

