/**
 * @file Sleep.h
 * @brief Low power and LPTIM timer.
 * @author Tomas Jakubik
 * @date Dec 11, 2020
 */

#ifndef SLEEP_H_
#define SLEEP_H_

#include <cstdint>
#include <stm32l0xx.h>

class Sleep
{
public:
    static TIM_HandleTypeDef htim21;   ///< HAL handle for TIM21, used for LSI calibration, necessary from interrupt

    static const constexpr uint32_t SUBSECONDS_N = 2;   ///< Subsecond resolution

    /**
     * @brief Time structure.
     */
    struct LpTime
    {
        uint32_t minute;
        uint32_t subsecond;

        LpTime(uint32_t minute, uint32_t subsecond) :
            minute(minute), subsecond(subsecond)
        {
        }

        /**
         * @brief Sanitize second overflows.
         */
        void sanitize()
        {
            if(subsecond >= (SUBSECONDS_N*60))
            {
                minute += subsecond / (SUBSECONDS_N*60);
                subsecond %= (SUBSECONDS_N*60);
            }
        }

        /**
         * @brief Operator to increment time.
         * @param rhs incremented value
         * @return reference to l value
         */
        LpTime& operator+=(const LpTime& rhs)
        {
            minute += rhs.minute;
            subsecond += rhs.subsecond;
            sanitize();
            return *this;
        }

        /**
         * @brief Operator to add two times.
         * @param rhs added value
         * @return new time object
         */
        LpTime operator+(const LpTime& rhs) const
        {
            LpTime new_time(minute + rhs.minute, subsecond + rhs.subsecond);
            new_time.sanitize();
            return new_time;
        }

        /**
         * @brief Compare two times.
         * @param rhs
         * @return true if left is earlier than right
         */
        bool operator<(const Sleep::LpTime &rhs)
        {
            return ((minute < rhs.minute)
                || ((minute == rhs.minute) && (subsecond < rhs.subsecond)));
        }

        /**
         * @brief Compare two times.
         * @param rhs
         * @return true if left is after right
         */
        bool operator>(const Sleep::LpTime &rhs)
        {
            return ((minute > rhs.minute)
                || ((minute == rhs.minute) && (subsecond > rhs.subsecond)));
        }

        /**
         * @brief Compare two times.
         * @param rhs
         * @return true if equal
         */
        bool operator==(const Sleep::LpTime &rhs)
        {
            return ((minute == rhs.minute) && (subsecond == rhs.subsecond));
        }

        inline bool operator<=(const LpTime& rhs){ return !(*this > rhs); }
        inline bool operator>=(const LpTime& rhs){ return !(*this < rhs); }
        inline bool operator!=(const LpTime& rhs){ return !(*this == rhs); }

    };

private:
    enum class LsiCalibrationStage
    {
        Off = 0,    ///< No calibration ongoing
        Edge1,      ///< Waiting for the first edge
        Edge2,      ///< Waiting for the second edge
        Finish,     ///< Calculating proper LPTIM settings and finishing
    };
    static volatile LsiCalibrationStage lsiStage;   ///< Current stage when calibrating LSI
    static volatile uint16_t lsiCapture1;   ///< Timer capture of the first LSI edge
    static volatile uint32_t lsiFrequency;  ///< LSI frequency

    static volatile uint32_t lpTimeMinutes; ///< Minute overloads of low-power time

    struct PlannedInterrupt
    {
        LpTime at;      ///< Time of planned interrupt
        bool active;    ///< Interrupt is planned if true
    };
    static volatile PlannedInterrupt plannedInterrupt;    ///< Remember interrupt time if it is in a future minute

public:
    Sleep() = delete;   //No instance
    ~Sleep() = delete;

    /**
     * @brief Store one capture of LSI signal.
     * @param capture TIM value captured for LSI edge
     */
    static void lsiTimCapture(uint16_t capture);

    /**
     * @brief Configures TIM21 to measure the LSI oscillator frequency and sets LPTIM accordingly.
     * @return LSI frequency, zero on error
     */
    static uint32_t calibrateLsi(void);

    /**
     * @brief Get calibrated LSI frequency.
     * @return LSI frequency [Hz]
     */
    static volatile uint32_t getLsiFrequency()
    {
        return lsiFrequency;
    }

    /**
     * @brief Get low-power time.
     * @return low power time from start
     */
    static LpTime getLpTime();

    /**
     * @brief Sleep until next interrupt.
     */
    static void sleep();

    /**
     * @brief Set interrupt to a given time.
     * @param interrupt time of interrupt
     */
    static bool setInterrupt(LpTime interrupt);

    /**
     * @brief Interrupt handler for LPTIM.
     */
    static void lptimInterruptHandler();

private:
    /**
     * @brief Set CMP value of LPTIM.
     * @param cmp new CMP value
     * @return true on success
     */
    static bool setCompare(uint16_t cmp);
};

#endif /* SLEEP_H_ */
