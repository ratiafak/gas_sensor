    /**
     * @file S2LP_CORE_SPI.c
     * @brief Provides interface between S2LP library and SPI.
     * @author Tomas Jakubik, inspired by examples provided with S2LP library
     * @date Nov 19, 2020
     */

#include "S2LP_CORE_SPI.h"
#include <stm32l0xx.h>
#include <stm32l0xx_hal_spi.h>
#include <stm32l0xx_ll_gpio.h>
#include <pinout.h>

#define HEADER_WRITE_MASK		0x00 /* !< Write mask for header byte */
#define HEADER_READ_MASK		0x01 /* !< Read mask for header byte */
#define HEADER_ADDRESS_MASK		0x00 /* !< Address mask for header byte */
#define HEADER_COMMAND_MASK		0x80 /* !< Command mask for header byte */

#define LINEAR_FIFO_ADDRESS		0xFF  /* !< Linear FIFO address */

#define S2LP_CMD_SIZE           2   ///< Size of the S2LP SPI exchange header
#define BUILT_HEADER(add_comm, w_r) (add_comm | w_r)  /*!< macro to build the header byte*/
#define WRITE_HEADER    BUILT_HEADER(HEADER_ADDRESS_MASK, HEADER_WRITE_MASK) /*!< macro to build the write header byte*/
#define READ_HEADER     BUILT_HEADER(HEADER_ADDRESS_MASK, HEADER_READ_MASK)  /*!< macro to build the read header byte*/
#define COMMAND_HEADER  BUILT_HEADER(HEADER_COMMAND_MASK, HEADER_WRITE_MASK) /*!< macro to build the command header byte*/


static SPI_HandleTypeDef* S2LPSpi_hspi = NULL;  ///< HAL handle to control SPI
static uint32_t S2LPSpi_timeout = 0;    ///< Timeout for SPI transfer and state pooling [HAL ticks]

/**
 * @brief Set HAL handle used for controlling SPI.
 * @param hspi HAL handle
 * @param timeout timeout for SPI transfer and state pooling [HAL ticks]
 */
void S2LPSpiSetHalHandle(SPI_HandleTypeDef* hspi, uint32_t timeout)
{
    S2LPSpi_hspi = hspi;
    S2LPSpi_timeout = timeout;
}

/**
 * @brief SPI control for S2LP library.
 * @param pHeader 2B header to send, modified by incoming data
 * @param pBuff data buffer to send, modified by incoming data
 * @param Length length of pBuff
 * @return zero on success, negative on error
 */
int32_t S2LPSpiWriteBuffer(uint8_t *pHeader, uint8_t *pBuff, uint16_t Length)
{
    HAL_StatusTypeDef success;

    LL_GPIO_ResetOutputPin(RF_CS_GPIO_Port, RF_CS_Pin);   //CS low
    success = HAL_SPI_TransmitReceive(S2LPSpi_hspi, pHeader, pHeader, 2, S2LPSpi_timeout);
    if (success == HAL_OK)
    {
        if (Length > 0)
        {
            success = HAL_SPI_TransmitReceive(S2LPSpi_hspi, pBuff, pBuff, Length, S2LPSpi_timeout);
        }
    }
    LL_GPIO_SetOutputPin(RF_CS_GPIO_Port, RF_CS_Pin);  //CS high

    return (success == HAL_OK) ? 0 : -1;  //Wrap to WriteBuffer negative value on error
}

/**
 * @brief SPI function to read registers from S2LP
 * @param address register address
 * @param n_bytes number of bytes to read
 * @param buffer put data here
 * @return radio status
 */
uint16_t S2LPSpiReadRegisters(uint8_t address, uint8_t n_bytes, uint8_t *buffer)
{
    uint8_t header[S2LP_CMD_SIZE] = { READ_HEADER, address };

    S2LPSpiWriteBuffer(header, buffer, n_bytes);

    return (((uint16_t)header[0]) << 8) | header[1];
}

/**
 * @brief SPI function to write registers of S2LP
 * @param address register address
 * @param n_bytes number of bytes to write
 * @param buffer contains data to write
 * @return radio status
 */
uint16_t S2LPSpiWriteRegisters(uint8_t address, uint8_t n_bytes, uint8_t *buffer)
{
    uint8_t header[S2LP_CMD_SIZE] = { WRITE_HEADER, address };

    S2LPSpiWriteBuffer(header, buffer, n_bytes);

    return (((uint16_t)header[0]) << 8) | header[1];
}

/**
 * @brief SPI function to send command to S2LP
 * @param command send this command
 * @return radio status
 */
uint16_t S2LPSpiCommandStrobes(uint8_t command)
{
    uint8_t header[S2LP_CMD_SIZE] = { COMMAND_HEADER, command };

    S2LPSpiWriteBuffer(header, NULL, 0);

    return (((uint16_t)header[0]) << 8) | header[1];
}

/**
 * @brief SPI function to read S2LP FIFO.
 * @param n_bytes number of bytes to read
 * @param buffer put data here
 * @return radio status
 */
uint16_t S2LPSpiReadFifo(uint8_t n_bytes, uint8_t *buffer)
{
    uint8_t header[S2LP_CMD_SIZE] = { READ_HEADER, LINEAR_FIFO_ADDRESS };

    S2LPSpiWriteBuffer(header, buffer, n_bytes);

    return (((uint16_t)header[0]) << 8) | header[1];
}

/**
 * @brief SPI function to write S2LP FIFO.
 * @param n_bytes number of bytes to write
 * @param buffer contains data to write, modified by incoming data
 * @return radio status
 */
uint16_t S2LPSpiWriteFifo(uint8_t n_bytes, uint8_t *buffer)
{
    uint8_t header[S2LP_CMD_SIZE] = { WRITE_HEADER, LINEAR_FIFO_ADDRESS };

    S2LPSpiWriteBuffer(header, buffer, n_bytes);

    return (((uint16_t)header[0]) << 8) | header[1];
}

