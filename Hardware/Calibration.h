/**
 * @file Calibration.h
 * @brief Provide calibration values stored in EEPROM.
 * @author Tomas Jakubik
 * @date Oct 5, 2020
 */

#ifndef CALIBRATION_H_
#define CALIBRATION_H_

#include <cstdint>

class Calibration
{
public:
    struct Data
    {
        int32_t freq_offset;   ///< Frequency offset of the radio [Hz]
    };

    static const constexpr uint32_t VERSION = 0x12345678;   ///< Version of calibration data
    static const uint32_t VERSION_ADDRESS; ///< Location of version
    static const uint32_t DATA_ADDRESS;    ///< Location of calibration data

private:
    ///Default calibration data
    const Data default_data =
    {
        .freq_offset = 0,
    };

public:
    Calibration(){} ///< Nothing to construct
    virtual ~Calibration(){}    ///< Nothing to destruct

    /**
     * @brief Get version of the configuration stored in EEPROM.
     * @return version code
     */
    uint32_t getVersion() const
    {
        return *(reinterpret_cast<uint32_t*>(VERSION_ADDRESS));
    }

    /**
     * @brief Check whether stored config is valid or whether default is used.
     * @return true if valid, false if default
     */
    bool isValid() const
    {
        return (getVersion() == VERSION);
    }

    /**
     * @brief Get calibration data stored in EEPROM or default calibration.
     * @return calibration data
     */
    const Data* getData() const
    {
        if (isValid())
        {
            return reinterpret_cast<const Data*>(DATA_ADDRESS);
        }
        else
        {
            return &default_data;
        }
    }

    /**
     * @brief Get default calibration data.
     * @return default calibration
     */
    const Data& getDefault() const
    {
        return default_data;
    }

    /**
     * @brief Write calibration.
     * @param new_data new calibration to write
     */
    void writeData(const Data &new_data);
};

#endif /* CALIBRATION_H_ */
