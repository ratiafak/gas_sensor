/**
 * @file Threshold.h
 * @brief Hardware control for comparator and DAC.
 * @author Tomas Jakubik
 * @date Mar 18, 2021
 */

#ifndef THRESHOLD_H_
#define THRESHOLD_H_

#include <global.hpp>

class Threshold
{
public:
    /**
     * @brief Interrupt handler of comparator.
     */
    static void compInterruptHandler()
    {
        disable();  //Disable itself to prevent rapid switching
    }

    /**
     * @brief Enable comparator interrupt.
     * @param polarity true to guard rise of current
     */
    static void enable(bool polarity)
    {
        if (LL_COMP_GetOutputPolarity(COMP1) != (polarity ? LL_COMP_OUTPUTPOL_NONINVERTED : LL_COMP_OUTPUTPOL_INVERTED))
        {
            disable();

            //Set polarity
            // non-inverted to trigger when lmp output rises above threshold
            // inverted to trigger when lmp output falls under threshold
            LL_COMP_SetOutputPolarity(COMP1, polarity ? LL_COMP_OUTPUTPOL_NONINVERTED : LL_COMP_OUTPUTPOL_INVERTED);
        }

        if (LL_COMP_IsEnabled(COMP1) == 0)
        {
            LL_COMP_Enable(COMP1);  //Enable COMP1
            LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_21);    //Clear pending flag
            LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_21);     //Enable EXTI wakeup
        }
    }

    /**
     * @brief Disable comparator interrupt.
     */
    static void disable()
    {
        if (LL_COMP_IsEnabled(COMP1))
        {
            LL_EXTI_DisableIT_0_31(LL_EXTI_LINE_21); //Disable EXTI wakeup
            LL_COMP_Disable(COMP1);  //Disable COMP1
        }
    }

    /**
     * @brief Start DAC, high current consumption.
     * @param value DAC value as ratio of vref [0 ~ 4095]
     */
    static void dac_start(uint16_t value)
    {
        HAL_DAC_SetValue(&global.hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, value);
        HAL_DAC_Start(&global.hdac, DAC_CHANNEL_1);
    }

    /**
     * @brief Stop DAC and return to low power consumption.
     */
    static void dac_stop()
    {
        HAL_DAC_Stop(&global.hdac, DAC_CHANNEL_1);
    }
};




#endif /* THRESHOLD_H_ */
