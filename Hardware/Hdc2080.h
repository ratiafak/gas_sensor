/**
 * @file Hdc2080.h
 * @brief Class to control HDC2080.
 * @author Tomas Jakubik
 * @date Sep 9, 2020
 */

#ifndef HARDWARE_HDC2080_H_
#define HARDWARE_HDC2080_H_

#include <stm32l0xx.h>
#include <stm32l0xx_hal_i2c.h>
#include <cstdint>
#include <cmath>

class Hdc2080
{
public:
    static const constexpr uint8_t ADDRESS_GND = 0x80; ///< I2C address for GND or floating address input
    static const constexpr uint8_t ADDRESS_VDD = 0x82; ///< I2C address for VDD address input

    enum class Precision : uint8_t
    {
        p09b = 0x2, ///< 9 bits of resolution
        p11b = 0x1, ///< 11 bits of resolution
        p14b = 0x0, ///< 14 bits of resolution
    };

private:
    static const constexpr uint8_t TEMPERATURE_LOW           = 0x00; ///< Temperature [7:0]
    static const constexpr uint8_t TEMPERATURE_HIGH          = 0x01; ///< Temperature [15:8]
    static const constexpr uint8_t HUMIDITY_LOW              = 0x02; ///< Humidity [7:0]
    static const constexpr uint8_t HUMIDITY_HIGH             = 0x03; ///< Humidity [15:8]
    static const constexpr uint8_t INTERRUPT_DRDY            = 0x04; ///< DataReady and interrupt configuration
    static const constexpr uint8_t TEMPERATURE_MAX           = 0x05; ///< Maximum measured temperature (Not supported in Auto Measurement Mode)
    static const constexpr uint8_t HUMIDITY_MAX              = 0x06; ///< Maximum measured humidity (Not supported in Auto Measurement Mode)
    static const constexpr uint8_t INTERRUPT_ENABLE          = 0x07; ///< Interrupt Enable
    static const constexpr uint8_t TEMP_OFFSET_ADJUST        = 0x08; ///< Temperature offset adjustment
    static const constexpr uint8_t HUM_OFFSET_ADJUST         = 0x09; ///< Humidity offset adjustment
    static const constexpr uint8_t TEMP_THR_L                = 0x0A; ///< Temperature Threshold Low
    static const constexpr uint8_t TEMP_THR_H                = 0x0B; ///< Temperature Threshold High
    static const constexpr uint8_t RH_THR_L                  = 0x0C; ///< Humidity threshold Low
    static const constexpr uint8_t RH_THR_H                  = 0x0D; ///< Humidity threshold High
    static const constexpr uint8_t RESET_DRDY_INT_CONF       = 0x0E; ///< Soft Reset and Interrupt Configuration
    static const constexpr uint8_t MEASUREMENT_CONFIGURATION = 0x0F; ///< Measurement configuration
    static const constexpr uint8_t MANUFACTURER_ID_LOW       = 0xFC; ///< Manufacturer ID Low
    static const constexpr uint8_t MANUFACTURER_ID_HIGH      = 0xFD; ///< Manufacturer ID High
    static const constexpr uint8_t DEVICE_ID_LOW             = 0xFE; ///< Device ID Low
    static const constexpr uint8_t DEVICE_ID_HIGH            = 0xFF; ///< Device ID High

    static const constexpr uint16_t MANUFACTURER_ID_VAL  = 0x5449; ///< Manufacturer ID value
    static const constexpr uint16_t DEVICE_ID_VAL        = 0x07d0; ///< Device ID value

    const uint16_t device_address;  ///< HDC2080 device I2C address, 7 left aligned bits
    const uint32_t i2c_timeout; ///< Timeout for I2C transfer
    const uint32_t ready_attempts; ///< Number of attempts to read reset status
    const uint8_t precision;  ///< Precision of temperature and humidity

    I2C_HandleTypeDef &hi2c;    ///< HAL I2C handle
    bool inited;    ///< True when the sensor is inited
    bool measurement_in_progress;   ///< True after trigger and before data ready
    uint16_t temperature;   ///< Last read temperature
    uint16_t humidity;      ///< Last read humidity

public:

    /**
     * @brief Configure library, do not touch I2C.
     * @param hi2c already initialized HAL I2C handle
     * @param device_address HDC2080 device I2C address, 7 left aligned bits
     * @param temperature_precision precision of temperature conversion
     * @param humidity_precision precision of humidity conversion
     * @param i2c_timeout timeout for I2C transfer [HAL ticks]
     * @param ready_attempts number of attempts to read AFE ready status
     */
    Hdc2080(I2C_HandleTypeDef &hi2c, uint16_t device_address,
            Precision temperature_precision = Precision::p14b, Precision humidity_precision = Precision::p14b,
            uint32_t i2c_timeout = 100, uint32_t ready_attempts = 4) :
        device_address(device_address),
        i2c_timeout(i2c_timeout),
        ready_attempts(ready_attempts),
        precision((static_cast<uint8_t>(temperature_precision) << 6) | (static_cast<uint8_t>(humidity_precision) << 4)),
        hi2c(hi2c),
        inited(false),
        measurement_in_progress(false),
        temperature(0),
        humidity(0)
    {
    }

    /**
     * @brief Nothing to destroy
     */
    virtual ~Hdc2080()
    {
    }

    /**
     * @brief Init the sensor and put to sleep.
     * @return true on success
     */
    bool init();

    /**
     * @brief Trigger a measurement.
     * @return true on success
     */
    bool measure();

    /**
     * @brief Readout measured values.
     * Call this after DRDY interrupt.
     * @return true on success
     */
    bool readout();

    /**
     * @brief Get temperature in full resolution.
     * @return temperature [deg C], NAN if temperature is not valid
     */
    float getTemperature();

    /**
     * @brief Get humidity in full resolution.
     * @return humidity [% RH], NAN if humidity is not valid
     */
    float getHumidity();

    /**
     * Get temperature with 0.1 degree resolution.
     * @return temperature [0.1 deg C], INT32_MIN if temperature is not valid
     */
    int32_t getTemperature0C1();

    /**
     * Get humidity with 1 % resolution.
     * @return humidity [% RH], INT32_MIN if humidity is not valid
     */
    int32_t getHumidityPercent();

    /**
     * @brief Know whether measurement is currently in progress
     * @return true if it is
     */
    bool isMeasurementInProgress() const
    {
        return measurement_in_progress;
    }
    
private:
    /**
     * @brief Write one register.
     * @param address register address
     * @param value register value
     * @return true on success
     */
    bool writeReg(uint8_t address, uint8_t value);
};

#endif /* HARDWARE_HDC2080_H_ */
