/**
 * @file S2LP_CORE_SPI.h
 * @brief Provides interface between S2LP library and SPI.
 * @author Tomas Jakubik, inspired by examples provided with S2LP library
 * @date Nov 19, 2020
 */

#ifndef __S2LP_CORE_SPI_H
#define __S2LP_CORE_SPI_H


#include <stdint.h>
#include <stm32l0xx.h>
#include <stm32l0xx_hal_spi.h>
#include <stm32l0xx_ll_gpio.h>
#include <stm32l0xx_ll_exti.h>
#include <pinout.h>

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Empty declaration because
 * "external front end module (aka power amplifier)" is not used.
 */
#define FEM_Operation(...)

/**
 * @brief Set HAL handle used for controlling SPI.
 * @param hspi HAL handle
 * @param timeout timeout for SPI transfer and state pooling [HAL ticks]
 */
void S2LPSpiSetHalHandle(SPI_HandleTypeDef* hspi, uint32_t timeout);

/**
 * @brief SPI control for S2LP library.
 * @param pHeader 2B header to send, modified by incoming data
 * @param pBuff data buffer to send, modified by incoming data
 * @param Length length of pBuff
 * @return zero on success, negative on error
 */
int32_t S2LPSpiWriteBuffer(uint8_t *pHeader, uint8_t *pBuff, uint16_t Length);

/**
 * @brief SPI function to read registers from S2LP
 * @param address register address
 * @param n_bytes number of bytes to read
 * @param buffer put data here
 * @return radio status
 */
uint16_t S2LPSpiReadRegisters(uint8_t address, uint8_t n_bytes, uint8_t *buffer);

/**
 * @brief SPI function to write registers of S2LP
 * @param address register address
 * @param n_bytes number of bytes to write
 * @param buffer contains data to write
 * @return radio status
 */
uint16_t S2LPSpiWriteRegisters(uint8_t address, uint8_t n_bytes, uint8_t *buffer);

/**
 * @brief SPI function to send command to S2LP
 * @param command send this command
 * @return radio status
 */
uint16_t S2LPSpiCommandStrobes(uint8_t command);

/**
 * @brief SPI function to read S2LP FIFO.
 * @param n_bytes number of bytes to read
 * @param buffer put data here
 * @return radio status
 */
uint16_t S2LPSpiReadFifo(uint8_t n_bytes, uint8_t *buffer);

/**
 * @brief SPI function to write S2LP FIFO.
 * @param n_bytes number of bytes to write
 * @param buffer contains data to write, modified by incoming data
 * @return radio status
 */
uint16_t S2LPSpiWriteFifo(uint8_t n_bytes, uint8_t *buffer);

#ifdef __cplusplus
}
#endif

#endif /*__S2LP_CORE_SPI_H*/

