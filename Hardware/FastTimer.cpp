/**
 * @file FastTimer.cpp
 * @brief Fast timer for less-than-ms timing.
 * @author Tomas Jakubik
 * @date Jul 1, 2021
 */

#include <FastTimer.h>
#include <Utils.hpp>
#include <stm32l062xx.h>
#include <stm32l0xx_ll_tim.h>
#include <stm32l0xx_ll_bus.h>

int FastTimer::count = 0;   ///< Count how many times the timer is used

/**
 * @brief Init timer.
 * @return true if hardware was initialized which took some time
 */
bool FastTimer::init()
{
    bool ret = false;

    Critical critical;
    if (used == false)
    {
        used = true;
        if (count == 0)  //First one inits the timer
        {
            //Enable timer clock
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_TIM22);

            //Timer configuration: continuous mode, one tick per us
            LL_TIM_InitTypeDef tim_init =
            {
                .Prescaler = static_cast<uint16_t>(SystemCoreClock / 1000000 - 1),
                .CounterMode = LL_TIM_COUNTERMODE_UP,
                .Autoreload = 0xffff,
                .ClockDivision = LL_TIM_CLOCKDIVISION_DIV1,
            };
            LL_TIM_Init(TIM22, &tim_init);

            //Start timer
            LL_TIM_EnableCounter(TIM22);

            ret = true; //Was initializing hardware
        }
        count++;
    }
    return ret;
}

/**
 * @brief Wait for a brief time.
 * @param us time to wait [us]
 */
void FastTimer::delay(uint16_t us)
{
    if (init())
    {
        //Initializing hardware took some time
        if (us > 20)
        {
            us -= 20;   //Subtract to compensate
        }
        else
        {
            return; //Already spent too long handling hardware
        }
    }

    //Wait
    uint16_t now = LL_TIM_GetCounter(TIM22);
    while (static_cast<uint16_t>(LL_TIM_GetCounter(TIM22) - now) < us);
}

/**
 * @brief Deinit timer.
 * Optionally deinit before destroying.
 */
void FastTimer::deinit()
{
    Critical critical;
    if (used)
    {
        used = false;
        if (count == 1)  //Last one deinits
        {
            //Deinitialize timer
            LL_TIM_DisableCounter(TIM22);
            LL_TIM_DeInit(TIM22);

            //Disable timer clock
            LL_APB2_GRP1_DisableClock(LL_APB2_GRP1_PERIPH_TIM22);
        }
        count--;
    }
}

